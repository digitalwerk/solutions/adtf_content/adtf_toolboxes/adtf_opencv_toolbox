# Release Notes

## Version History

- **1.0.0**
  - Initial version of components that enables OpenCV within ADTF to process, encode, decode and transform video and audio data
  - Provide SDK with adtf::opencvtb::cOpenCVBaseFilter to create own OpenCV based filters
  - Use ADTF 3.19
  - Use OpenCV 4.5.5

---

## Known Issues / Restrictions

- none (initial version)
