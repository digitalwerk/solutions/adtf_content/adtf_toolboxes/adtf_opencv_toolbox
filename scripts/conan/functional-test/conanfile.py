# Copyright 2024 Digitalwerk GmbH.
#
#     This Source Code Form is subject to the terms of the Mozilla
#     Public License, v. 2.0. If a copy of the MPL was not distributed
#     with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
#
# If it is not possible or desirable to put the notice in a particular file, then
# You may include the notice in a location (such as a LICENSE file in a
# relevant directory) where a recipient would be likely to look for such a notice.
#
# You may add additional accurate notices of copyright ownership.

from conans import ConanFile, tools, CMake
from conans.errors import ConanException
import os

class FunctionalTests(ConanFile):
    name = "functional_tests"
    options = {"pkg_reference": "ANY"}

    def requirements(self):
         self.requires(str(self.options.pkg_reference))

    def build(self):
        with open('functional_test_results.junit.xml', 'w') as f:
            f.write('<testsuites><testsuite tests="1"><testcase name="Package available"/></testsuite></testsuites>')
