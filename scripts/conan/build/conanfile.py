# Copyright 2024 Digitalwerk GmbH.
#
#     This Source Code Form is subject to the terms of the Mozilla
#     Public License, v. 2.0. If a copy of the MPL was not distributed
#     with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
#
# If it is not possible or desirable to put the notice in a particular file, then
# You may include the notice in a location (such as a LICENSE file in a
# relevant directory) where a recipient would be likely to look for such a notice.
#
# You may add additional accurate notices of copyright ownership.

from conans import ConanFile, tools, __version__ as conan_version
from conan.tools.cmake import CMake, CMakeToolchain, CMakeDeps
from conans.errors import ConanException
import os
import re

class Build(ConanFile):
    name = "adtf_opencv_toolbox"
    label = "ADTF OpenCV Toolbox"
    description = "Enhance ADTF to capturing, processing, encoding, decoding and transforming image, audio and video based data using OpenCV."
    url = "https://gitlab.com/digitalwerk/solutions/adtf_content/adtf_toolboxes/adtf_opencv_toolbox.git"
    homepage = "https://gitlab.com/digitalwerk/solutions/adtf_content/adtf_toolboxes/adtf_opencv_toolbox"
    license = "MPL-2.0"
    
    settings = "os", "arch", "compiler", "build_type"

    options = {"with_cuda": [True, False]}

    default_options = { "with_cuda": False }

    keep_imports = True

    tool_requires = ["cmake/3.23.2@dw/stable",
                    "doxygen/1.9.1@fep/stable",
                    "graphviz/2.47.3@fep/stable",
                    "cmake_toolchain_helper/1.1.1@dw/stable"]

    requires = [("adtf_sdk/3.19.0@dw/stable", "private"),
                ("opencv/4.5.5", "private"),
                ("openssl/1.1.1j", "private")]

    def package_id(self):
        if self.info.settings.build_type != "Debug":
            self.info.settings.build_type = "AnyReleaseBuild"

    def configure(self):
        self.options["opencv"].with_ffmpeg = False

        if self.settings.os == "Linux":
            self.options["opencv"].with_v4l = True
            self.options["opencv"].with_gtk = False

        if self.options.with_cuda:
            self.options["opencv"].contrib = True
            self.options["opencv"].with_cuda = True
            self.options["opencv"].with_cudnn = True
            self.options["opencv"].dnn_cuda = True
            self.options["opencv"].with_cublas = True

    def generate(self):
        tc = CMakeToolchain(self)

        if re.match(r'^([\d]+).([\d]+).([\d]+)$', str(self.version)):
            version_string = self.version
        else:
            version_string = '1.99.99'

        tc.cache_variables["LABEL_NAME"] = self.label
        tc.cache_variables["BUILD_VERSION"] = version_string
        tc.cache_variables["GIT_COMMIT"] = os.environ.get("CI_COMMIT_SHA", "local")

        tc.cache_variables["ADTF_EXTRACT_DEBUG_INFO"] = True
        tc.cache_variables["ADTF_TESTING_USE_CATCH_DISCOVER"] = True

        crosscompiling_emulator = os.environ.get("CONAN_CROSSCOMPILING_EMULATOR")
        if crosscompiling_emulator:
            tc.cache_variables["CMAKE_CROSSCOMPILING_EMULATOR"] = str(crosscompiling_emulator)
        tc.cache_variables["CMAKE_CONFIGURATION_TYPES"] = str(self.settings.build_type)
        tc.cache_variables["CMAKE_INSTALL_MESSAGE"] = "NEVER"

        tc.generate()

        deps = CMakeDeps(self)

        deps.generate()

    def build(self):
        cmake = CMake(self)

        if self.should_configure:
            cmake.configure()

        if self.should_build:
            cmake.build()

        if self.should_install:
            cmake.install()

        if self.should_test:
            ctest_call = "ctest -C " + str(self.settings.build_type) + " -T test --no-compress-output --output-on-failure --output-junit test_results.junit.xml"
            self.run(ctest_call, cwd=self.build_folder, ignore_errors=True)

    def package(self):
        pass

    def package_info(self):
        self.cpp_info.set_property("cmake_find_mode", "none")
        self.cpp_info.builddirs = ["lib/cmake/opencv_base_filter"]
