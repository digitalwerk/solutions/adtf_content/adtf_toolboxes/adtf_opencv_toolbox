# ADTF OpenCV Toolbox

## Description

This package contains components to extend `ADTF` for capturing, processing, encoding, decoding and transforming image, audio and video based data using [OpenCV](https://opencv.org/).

With OpenCV, ADTF gets access for image procession and computer vision. You will be able to access data from camera or files, resize images, convert between images and mat and access a deep neural networks.

> Note: The toolbox is open source. Feel free to adapt the delivered components, share contribution and/or create own customizations using OpenCV and the adtf::opencvtb::cOpenCVBaseFilter

Please have a look at [Release Notes](RELEASENOTES.md)

## How to build

> If for any reason you do not make use of our delivered prebuild packages, we recommend using `cmake` together with `conan` on which the source code is based. You will find the required `scripts/conan/build/conanfile.py` within the source package.

### Build Environment

The libraries are build and tested only under following compilers and operating systems:

#### Windows Desktop (x86_64)

> We recommend our used and tested platform Microsoft Windows 10 64 Bit. On any other Windows 64 Bit platform (e.g. Windows 7 or 11) make sure that it fulfills the following requirements

- Visual C++ 2019 (at least CXX Compiler MSVC 19.29) / Visual C++ 14.2 (VC142 Toolchain at least 14.29)
- MSBuild Version 16.x (at least 16.11)
- MSBuild Tools only or full Visual Studio IDE installation which covers this requirements
- Windows Kit 10 (at least 10.0.19041.0)
- CMake >= 3.23.2

#### Linux Desktop (x86_64)

> We recommend our used and tested platform Ubuntu 18.04 LTS 64 Bit. On any other distributions (e.g. Fedora, Debian) make sure that it fulfills the following requirements

- gcc >= 7
- libc >= 2.27
- libstdc++ >= 6.0.25
- CMake >= 3.23.2
- build-essential libarchive-dev mesa-common-dev mesa-utils libglib2.0-0 libxcb-xinerama0

## License Information

For any license information have a look at [LICENSE](LICENSE).

Furthermore please refer to [Used Open Source Software](doc/license/3rd_party_licenses.md) for an overview of 3rd party packages we are glad to use.
