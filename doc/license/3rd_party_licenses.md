# Used Open Source Software

For creation of our artifacts we make use of several 3rd party content.

## Build and Runtime Dependencies

Open Source Software we are very glad to use:

|Libraries|Version|License|Homepage|Repository|
|-|-|-|-|-|
|catch2|2.13.7|BSL-1.0|[link](https://github.com/catchorg/Catch2)|[link](https://github.com/catchorg/Catch2.git)|
|opencv|4.5.5|Apache-2.0|[link](https://opencv.org)|[link](https://github.com/opencv/opencv.git)|
|openssl|1.1.1j|OpenSSL|[link](https://www.openssl.org/)|[link](https://github.com/openssl/openssl.git)|

## Build Tools

Following build tools helps us to compile, document and package our delivery:

|Libraries|Version|License|Homepage|Repository|
|-|-|-|-|-|
|cmake|3.23.2|BSD-3-Clause|[link](https://cmake.org/)|[link](https://gitlab.kitware.com/cmake/cmake)|
|cmake_toolchain_helper|1.1.1|MPL-2.0|[link](https://gitlab.com/digitalwerk/solutions/conan/cmake_toolchain_helper)|[link](https://gitlab.com/digitalwerk/solutions/conan/cmake_toolchain_helper.git)|
|conan|1.59.0|MIT|[link](https://conan.io/)|[link](https://github.com/conan-io/conan)|
|doxygen|1.9.1|GPL-2.0|[link](https://www.doxygen.nl/index.html)|[link](https://github.com/doxygen/doxygen.git)|
|graphviz|2.47.3|EPL-v10|[link](https://graphviz.org/)|[link](https://gitlab.com/graphviz/graphviz)|
