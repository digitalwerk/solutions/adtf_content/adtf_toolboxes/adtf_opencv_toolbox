/**
 *
 * @file
 * Copyright 2024 Digitalwerk GmbH.
 *
 *     This Source Code Form is subject to the terms of the Mozilla
 *     Public License, v. 2.0. If a copy of the MPL was not distributed
 *     with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * If it is not possible or desirable to put the notice in a particular file, then
 * You may include the notice in a location (such as a LICENSE file in a
 * relevant directory) where a recipient would be likely to look for such a notice.
 *
 * You may add additional accurate notices of copyright ownership.
 */

#include <opencv_base_filter/opencv_samplebuffer.h>
#include <opencv_base_filter/opencv_base_filter.h>

using namespace adtf::util;
using namespace adtf::ucom;
using namespace adtf::base;
using namespace adtf::streaming;
using namespace adtf::filter;

namespace adtf {
namespace opencvtb {

struct MatUtil
{
    static bool isEqualViewport(cv::Mat const& A, cv::Mat const& B)
    {
        if (A.flags != B.flags) return false;           // Test if format changed e.g. RGB to BGR
        if (A.data != B.data) return false;             // Test if viewport x and y are equal
        if (A.dims != B.dims) return false;             // Test if matrix dimensions are equal
        if (A.cols != B.cols) return false;             // Test if viewport w is equal
        if (A.rows != B.rows) return false;             // Test if viewport h is equal
        if (A.step != B.step) return false;             // Test matrix layout equality
        if (A.type() != B.type()) return false;         // Test if element type is equal
        if (A.elemSize() != B.elemSize()) return false; // Test if element size is equal
        return true;
    }
};

cOpenCVBaseFilter::cOpenCVBaseFilter()
{
    m_bOnlyLastSample.SetDescription("Option for dropping samples.");
    RegisterPropertyVariable("only_last_sample", m_bOnlyLastSample);

    object_ptr<IStreamType> pStreamType = make_object_ptr<cStreamType>(stream_meta_type_image());
    SetDescription("mat_out", "Result of image processing.");
    m_pOutput = CreateOutputPin("mat_out", pStreamType);
    SetDescription("mat_in", "Source for image processing.");
    m_pInput = CreateInputPin("mat_in", pStreamType);
    m_pInput->SetAcceptTypeCallback([this](const auto & pStreamType) -> tResult
    {
        RETURN_IF_FAILED(get_stream_type_image_format(m_sInputFormat, *pStreamType.Get()));

        if (m_sInputFormat.m_strFormatName == ADTF_IMAGE_FORMAT(GREYSCALE_8))
        {
            m_nMatType = CV_8UC1;
        }
        else if (m_sInputFormat.m_strFormatName == ADTF_IMAGE_FORMAT(RGB_24) ||
                 m_sInputFormat.m_strFormatName == ADTF_IMAGE_FORMAT(BGR_24))
        {
            m_nMatType = CV_8UC3;
        }
        else
        {
            RETURN_ERROR_DESC(ERR_NOT_SUPPORTED, "Unsupported image format (%s), must be one of (GREYSCALE_8, RGB_24, BGR_24)",
                m_sInputFormat.m_strFormatName.GetPtr() ? m_sInputFormat.m_strFormatName.GetPtr() : "");
        }

        auto sOutputFormat = this->ConvertImageFormat(m_sInputFormat);
        if (sOutputFormat != m_sInputFormat)
        {
            object_ptr<IStreamType> pNewImageType = make_object_ptr<cStreamType>(stream_meta_type_image());
            RETURN_IF_FAILED(set_stream_type_image_format(*pNewImageType.Get(), sOutputFormat));
            RETURN_IF_FAILED(m_pOutput->ChangeType(pNewImageType));
        }
        RETURN_NOERROR;
    });
}

tStreamImageFormat cOpenCVBaseFilter::ConvertImageFormat(const tStreamImageFormat & oImageFormat)
{
    return oImageFormat;
}

tResult cOpenCVBaseFilter::ProcessInput(ISampleReader* pReader, iobject_ptr<const ISample> const& pSample)
{
    // TODO: Only dnn_filter.hpp needs this variable, try to remove it.
    if (!m_nMatType.has_value())
    {
        RETURN_ERROR_DESC(ERR_INVALID_ARG, "!m_nMatType.has_value()");
    }

    // Get input sample.
    // TODO: - The GetLastSample() function is fishy, 
    //         because there is no assignment to 'm_pLastValidSample'.
    //       - The last check for 'pInputSample' is also fishy, 
    //         because why should GetLastSample() return ok and return nullptr at same time?
    if (m_bOnlyLastSample && IS_OK(pReader->GetLastSample(m_pInputSample)) && m_pInputSample)
    {

    }
    else
    {
        m_pInputSample = pSample;
    }
    if (!m_pInputSample)
    {
        RETURN_ERROR_DESC(ERR_FAILED, "No input sample.");
    }

    // Get input sample-buffer.
    object_ptr_shared_locked<const ISampleBuffer> pInputSampleBuffer;
    if (IS_FAILED(m_pInputSample->Lock(pInputSampleBuffer)) || !pInputSampleBuffer)
    {
        RETURN_ERROR_DESC(ERR_FAILED, "No input sample buffer.");
    }

    // Validate input sample-buffer.
    if (pInputSampleBuffer->GetSize() != m_sInputFormat.m_szMaxByteSize)
    {
        RETURN_ERROR_DESC(ERR_NOT_SUPPORTED,
            "received sample size miss match %" PRIu64 " != %" PRIu64,
            static_cast<uint64_t>(pInputSampleBuffer->GetSize()),
            static_cast<uint64_t>(m_sInputFormat.m_szMaxByteSize));
    }

    m_oInputMat = cv::Mat(
        m_sInputFormat.m_ui32Height,
        m_sInputFormat.m_ui32Width,
        m_nMatType.value(),
        const_cast<void*>(pInputSampleBuffer->GetPtr()),
        pInputSampleBuffer->GetSize() / m_sInputFormat.m_ui32Height);

    if (m_oInputMat.empty())
    {
        m_oInputMat = cv::Mat();
        RETURN_NOERROR; // Nothing todo.
    }
    
    std::string oInputFormatName = m_sInputFormat.m_strFormatName.GetPtr() ? m_sInputFormat.m_strFormatName.GetPtr() : "";
    if (oInputFormatName.empty())
    {
        LOG_WARNING("Got empty input format name");
    }

    // Do actual filter pass work. Transform input cv::Mat to output cv::Mat.
    // Do update outputpin-format(s) if necessary, because func has access to all necessary format information.
    tNanoSeconds oInputTimestamp = get_sample_time(m_pInputSample);

    auto const errorCode = ProcessMat(m_oInputMat, oInputTimestamp, oInputFormatName);

    m_pInputSample = {}; // We don't need it anymore. And we don't wait for another call to this func.
    m_oInputMat = cv::Mat(); // We don't need it anymore. And we don't wait for another call to this func.

    if (IS_FAILED(errorCode))
    {
        RETURN_ERROR_DESC(errorCode,"ProcessMat() failed.");
    }

    RETURN_NOERROR;
}

tResult cOpenCVBaseFilter::Init(tInitStage eStage)
{
    RETURN_IF_FAILED(cFilter::Init(eStage));

    switch (eStage)
    {
    case tInitStage::StageFirst:
    {
        RETURN_IF_FAILED(OnStageFirst());
    }
    break;
    case tInitStage::StagePreConnect:
    {
        RETURN_IF_FAILED(OnStagePreConnect());
    }
    break;
    case tInitStage::StagePostConnect:
    {
        RETURN_IF_FAILED(OnStagePostConnect());
    }
    break;
    default:
        break;
    }

    RETURN_NOERROR;
}

tResult cOpenCVBaseFilter::WriteMat(::adtf::filter::cPinWriter* pPin, ::cv::Mat const& oMat,
                                    ::adtf::base::tNanoSeconds oTimestamp, ::std::string const& oFormatName)
{
    if (!pPin)
    {
        THROW_ERROR_DESC(ERR_FAILED, "No output-pin.");
    }

    if (oMat.empty())
    {
        RETURN_NOERROR; // Nothing todo.
    }

    cv::Mat oDstMat = oMat;
    bool bCanRelay = false;

    // These 2 checks here determine if the .data pointer changed and or the viewport.
    // ProcessMat() has to give one other guarantee, that it clones before
    // any change ( e.g. drawing to the viewport ) is done.
    // If nothing changed:
    //    + Same Data Pointer ( checked here )
    //    + Same Viewport ( checked here )
    //    + Nothing drawn to Viewport without cloning before ( guaranteed by ProcessMat() )
    // then we have detected a Relay case.

    // Relay case:
    if (MatUtil::isEqualViewport(m_oInputMat, oDstMat))
    {
        bCanRelay = true;        
    }
    // New predicate for managed ref count detection.    
    else if (oMat.u == nullptr || ((oMat.u->flags & cv::UMatData::USER_ALLOCATED) != 0))
    {
        LOG_DETAIL("oMat.u == nullptr || ((oMat.u->flags & cv::UMatData::USER_ALLOCATED) != 0)");
        oDstMat = oMat.clone();
    }

    // ======================
    // pin->ChangeType():
    // ======================
    // We do this before any writing to make sure the Pin is configured. ( Also for RelayCase )

    tStreamImageFormat oNewFmt;
    tStreamImageFormat oOldFmt;

    object_ptr<const IStreamType> pOldType;
    if (IS_FAILED(pPin->GetType(pOldType)))
    {
        std::string oPinName;
        pPin->GetName(adtf_string_intf(oPinName));
        RETURN_ERROR_DESC(ERR_FAILED, "Can't get stream-type from output-pin '%s'.", oPinName.c_str());
    }

    if (IS_FAILED(get_stream_type_image_format(oOldFmt, *pOldType)))
    {
        std::string oPinName;
        pPin->GetName(adtf_string_intf(oPinName));
        RETURN_ERROR_DESC(ERR_FAILED, "Can't get image-format from output-pin '%s'.", oPinName.c_str());
    }

    if (oFormatName.empty())
    {
        oNewFmt.m_strFormatName = oOldFmt.m_strFormatName.GetPtr() ? oOldFmt.m_strFormatName.GetPtr() : "";
    }
    else
    {
        oNewFmt.m_strFormatName = oFormatName.c_str();
    }
    if (oNewFmt.m_strFormatName.IsEmpty())
    {
        switch (oMat.channels())
        {
            case 1: oNewFmt.m_strFormatName = ADTF_IMAGE_FORMAT(GREYSCALE_8); break;
            case 3: oNewFmt.m_strFormatName = ADTF_IMAGE_FORMAT(BGR_24); break;
            case 4: oNewFmt.m_strFormatName = ADTF_IMAGE_FORMAT(BGRA_32); break;
            default: {
                std::string oPinName;
                pPin->GetName(adtf_string_intf(oPinName));
                THROW_ERROR_DESC(ERR_FAILED, "No valid output-format detected for output-pin '%s'.", oPinName.c_str());
            }
        }
    }
    oNewFmt.m_ui32Width = static_cast<uint32_t>(oMat.cols);
    oNewFmt.m_ui32Height = static_cast<uint32_t>(oMat.rows);
    oNewFmt.m_szMaxByteSize = static_cast<size_t>(oMat.step) * oNewFmt.m_ui32Height;

    if (oNewFmt != oOldFmt)
    {
        object_ptr<IStreamType> pNewType = make_object_ptr<cStreamType>(stream_meta_type_image());
        RETURN_IF_FAILED(set_stream_type_image_format(*pNewType.Get(), oNewFmt));
        RETURN_IF_FAILED(pPin->ChangeType(pNewType));
    }

    // ======================
    // pin->Write():
    // ======================
    object_ptr<ISample> pSample;
    RETURN_IF_FAILED(::adtf::streaming::alloc_sample(pSample, oTimestamp));

    if (bCanRelay)
    {
        if (!m_pInputSample)
        {
            RETURN_ERROR_DESC(ERR_FAILED, "No input sample.");
        }

        object_ptr<const ISample> pInputSample(m_pInputSample);
        object_ptr_shared_locked<const ISampleBuffer> pInputBuffer;

        RETURN_IF_FAILED(m_pInputSample->Get(*pSample.Get()));
        
        set_sample_time(*pSample.Get(),oTimestamp);

        RETURN_IF_FAILED(pPin->Write(m_pInputSample));
    }
    else
    {
        if (IS_FAILED(createOpenCVSample(pSample, oDstMat, oTimestamp)))
        {
            std::string oPinName;
            pPin->GetName(adtf_string_intf(oPinName));
            RETURN_ERROR_DESC(ERR_FAILED, "No sample created for output-pin '%s'.", oPinName.c_str());
        }

        if (IS_FAILED(pPin->Write(pSample)))
        {
            std::string oPinName;
            pPin->GetName(adtf_string_intf(oPinName));
            RETURN_ERROR_DESC(ERR_FAILED, "No sample written to output-pin '%s'.", oPinName.c_str());
        }
    }

    RETURN_NOERROR;
}

}
}
