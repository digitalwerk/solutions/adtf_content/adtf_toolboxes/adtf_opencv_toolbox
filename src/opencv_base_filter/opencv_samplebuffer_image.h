/**
 *
 * @file
 * Copyright 2024 Digitalwerk GmbH.
 *
 *     This Source Code Form is subject to the terms of the Mozilla
 *     Public License, v. 2.0. If a copy of the MPL was not distributed
 *     with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * If it is not possible or desirable to put the notice in a particular file, then
 * You may include the notice in a location (such as a LICENSE file in a
 * relevant directory) where a recipient would be likely to look for such a notice.
 *
 * You may add additional accurate notices of copyright ownership.
 */

#pragma once
#include <opencv2/core/mat.hpp>
#include <adtf_streaming3.h>

namespace adtf {
namespace opencvtb {

/**
*   cOpenCVImageSampleBuffer
*/

class cOpenCVImageSampleBuffer final : public ucom::object<::adtf::streaming::ant::ISampleBuffer>
{
    //! Disabled copy constructor
    cOpenCVImageSampleBuffer(cOpenCVImageSampleBuffer const& oOther) = delete;
    //! Disabled move constructor
    cOpenCVImageSampleBuffer(cOpenCVImageSampleBuffer&& oOther) = delete;
    //! Disabled copy operator
    cOpenCVImageSampleBuffer& operator=(cOpenCVImageSampleBuffer const& oOther) = delete;
    //! Disabled move operator
    cOpenCVImageSampleBuffer& operator=(cOpenCVImageSampleBuffer&& oOther) = delete;

public:
    /**
     * Value constructor.
     *
     * @param[in] oMat The OpenCV matrix to be stored inside this class instance.
     *
     */
    cOpenCVImageSampleBuffer(cv::Mat const& oMat);

    /**
     * Destructor.
     */
    ~cOpenCVImageSampleBuffer() override;

    /**
     * Direct Writing Access.
     * Retrieves the raw buffer content pointer.
     *
     * @warning Do not write more than GetSize() bytes !
     * @return the buffers raw pointer.
     *
     * @note This class throws exception ERR_NOT_SUPPORTED.
     */
    void* GetPtr() override;

    /**
     * Direct Reading Access.
     * Retrieves the raw buffer content pointer.
     *
     * @return the buffers raw pointer.
     */
    const void* GetPtr() const override;

    /**
     * Returns the current size in bytes.
     *
     * @return Byte count of valid/used memory.
     */
    size_t GetSize() const override;

    /**
     * Returns the current maximal capacity in bytes.
     *
     * @return Byte count of allocated memory.
     *
     * @note This class always returns @ref GetSize().
     */
    size_t GetCapacity() const override;

    /**
     * Copy Writing Access. IRawMemory -> SampleBuffer
     * The given memory will be copied to the buffer.
     * Depending on implementation of the buffer, it will grow automatically.
     *
     * @param[in] oMemory Memory to copy from.
     *
     * @return Standard Result Code.
     *
     * @note This class always returns ERR_NOT_SUPPORTED.
     */
    tResult Write(::adtf::base::ant::IRawMemory const& oMemory) override;

    /**
     * Copy Reading Access. SampleBuffer -> IRawMemory
     * The buffer content will be copied to the given memory.
     * Depending on implementation of the buffer, it will grow automatically.
     *
     * @param[in,out] oMemory Memory to copy to.
     *
     * @return Standard Result Code.
     *
     * @note This class always returns ERR_NOT_SUPPORTED.
     */
    tResult Read(::adtf::base::ant::IRawMemory&& oMemory) const override;

    /**
     * Reserves memory.
     * Capacity will be at least szSize bytes.
     * Current data will be lost.
     *
     * @param[in] oByteCount Size in bytes.
     *
     * @return standard result.
     *
     * @note This class always returns ERR_NOT_SUPPORTED.
     */
    tResult Reserve(size_t oByteCount) override;

    /**
     * Resizes the Buffer.
     * Capacity will be at least szSize bytes.
     * Current data will be retained.
     *
     * @param[in] oByteCount Size in bytes.
     *
     * @return standard result.
     *
     * @note This class always returns ERR_NOT_SUPPORTED.
     */
    tResult Resize(size_t oByteCount) override;

    /**
     * Locks the object for exclusive use.
     * For your own convinience use object_ptr_locked as lock guard!
     *
     * @retval ERR_NOERROR succeeded lock. DO NOT Forget to unlock!
     *
     * @note This class does nothing and always returns ERR_NOERROR.
     */
    tResult Lock() const override;

    /**
     * Unlocks the object for exclusive use.
     * For your own convinience use object_ptr_locked as lock guard!
     *
     * @retval ERR_NOERROR succeeded unlock.
     *
     * @note This class does nothing and always returns ERR_NOERROR.
     */
    tResult Unlock() const override;

    /**
     * Locks the object for shared use.
     * For your own convinience use object_ptr_shared_locked as lock guard!
     *
     * @retval ERR_NOERROR succeeded lock. DO NOT Forget to unlock!
     *
     * @note This class does nothing and always returns ERR_NOERROR.
     */
    tResult LockShared() const override;

    /**
     * Unlocks the object for shared use.
     * For your own convinience use object_ptr_shared_locked as lock guard!
     *
     * @retval ERR_NOERROR succeeded unlock.
     *
     * @note This class does nothing and always returns ERR_NOERROR.
     */
    tResult UnlockShared() const override;

public:
    //! The OpenCV Mat
    cv::Mat m_oMat;
};

} // end namespace opencvtb.
} // end namespace adtf.

