/**
 *
 * @file
 * Copyright 2024 Digitalwerk GmbH.
 *
 *     This Source Code Form is subject to the terms of the Mozilla
 *     Public License, v. 2.0. If a copy of the MPL was not distributed
 *     with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * If it is not possible or desirable to put the notice in a particular file, then
 * You may include the notice in a location (such as a LICENSE file in a
 * relevant directory) where a recipient would be likely to look for such a notice.
 *
 * You may add additional accurate notices of copyright ownership.
 */

#include "opencv_samplebuffer_image.h"

namespace adtf {
namespace opencvtb {

cOpenCVImageSampleBuffer::cOpenCVImageSampleBuffer(cv::Mat const& oMat)
    : m_oMat(oMat)
{
}

cOpenCVImageSampleBuffer::~cOpenCVImageSampleBuffer()
{
}

void* cOpenCVImageSampleBuffer::GetPtr()
{
    THROW_ERROR_DESC(ERR_NOT_SUPPORTED, "Direct write access is forbidden because of internal reference counting");
}

const void* cOpenCVImageSampleBuffer::GetPtr() const
{
    return m_oMat.data;
}

size_t cOpenCVImageSampleBuffer::GetSize() const
{
    return static_cast<size_t>(m_oMat.step) * static_cast<size_t>(m_oMat.rows);
}

size_t cOpenCVImageSampleBuffer::GetCapacity() const
{
    return GetSize();
}

tResult cOpenCVImageSampleBuffer::Write(::adtf::base::ant::IRawMemory const& /* oMemory */)
{
    RETURN_ERROR(ERR_NOT_SUPPORTED);
}

tResult cOpenCVImageSampleBuffer::Read(::adtf::base::ant::IRawMemory&& /* oMemory */) const
{
    RETURN_ERROR(ERR_NOT_SUPPORTED);
}

tResult cOpenCVImageSampleBuffer::Reserve(size_t /* oByteCount */)
{
    RETURN_ERROR(ERR_NOT_SUPPORTED);
}

tResult cOpenCVImageSampleBuffer::Resize(size_t /* oByteCount */)
{
    RETURN_ERROR(ERR_NOT_SUPPORTED);
}

tResult cOpenCVImageSampleBuffer::Lock() const
{
    RETURN_NOERROR;
}

tResult cOpenCVImageSampleBuffer::Unlock() const
{
    RETURN_NOERROR;
}

tResult cOpenCVImageSampleBuffer::LockShared() const
{
    RETURN_NOERROR;
}

tResult cOpenCVImageSampleBuffer::UnlockShared() const
{
    RETURN_NOERROR;
}

} // end namespace opencvtb.
} // end namespace adtf.