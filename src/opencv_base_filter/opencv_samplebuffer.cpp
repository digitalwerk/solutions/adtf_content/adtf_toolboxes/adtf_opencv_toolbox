/**
 *
 * @file
 * Copyright 2024 Digitalwerk GmbH.
 *
 *     This Source Code Form is subject to the terms of the Mozilla
 *     Public License, v. 2.0. If a copy of the MPL was not distributed
 *     with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * If it is not possible or desirable to put the notice in a particular file, then
 * You may include the notice in a location (such as a LICENSE file in a
 * relevant directory) where a recipient would be likely to look for such a notice.
 *
 * You may add additional accurate notices of copyright ownership.
 */

#include <opencv_base_filter/opencv_samplebuffer.h>
#include "opencv_samplebuffer_image.h"

namespace adtf {
namespace opencvtb {

tResult createOpenCVSample( ::adtf::ucom::object_ptr<::adtf::streaming::ISample>& pSample,
                            ::cv::Mat const& oMat,
                            ::adtf::base::tNanoSeconds const& oTimestamp )
{
    RETURN_IF_FAILED(::adtf::streaming::alloc_sample(pSample, oTimestamp));

    ::adtf::ucom::object_ptr<::adtf::streaming::ISampleBuffer> pSampleBuffer
        = ::adtf::ucom::make_object_ptr<cOpenCVImageSampleBuffer>(oMat);

    if (!pSampleBuffer)
    {
        RETURN_ERROR_DESC(ERR_MEMORY, "Can't create sample-buffer.");
    }

    ::adtf::ucom::object_ptr<::adtf::streaming::thor::ISample> pThorSample(pSample);
    RETURN_IF_FAILED(pThorSample->SetSampleBuffer(pSampleBuffer));

    RETURN_NOERROR;
}

bool isSameFormat( ::adtf::streaming::tStreamImageFormat const& a, 
                   ::adtf::streaming::tStreamImageFormat const& b )
{
    if (a.m_ui32Height != b.m_ui32Height) return false;
    if (a.m_ui32Width != b.m_ui32Width) return false;
    if (a.m_szMaxByteSize != b.m_szMaxByteSize) return false;
    if (a.m_strFormatName != b.m_strFormatName) return false;
    //if (a.m_ui8DataEndianess != b.m_ui8DataEndianess) return false;
    return true;
}

::adtf::streaming::tStreamImageFormat createCompatibleImageStreamFormat(::cv::Mat const& oMat)
{
    ::adtf::streaming::tStreamImageFormat oFmt;

    if (oMat.type() == CV_8UC3) // Most likely
    {
        oFmt.m_strFormatName = ADTF_IMAGE_FORMAT(BGR_24);
    }
    else if (oMat.type() == CV_8UC1)
    {
        oFmt.m_strFormatName = ADTF_IMAGE_FORMAT(GREYSCALE_8);
    }
    else if (oMat.type() == CV_8UC4)
    {
        oFmt.m_strFormatName = ADTF_IMAGE_FORMAT(BGRA_32);
    }
    else
    {
        // Can't find compatible image-stream-format-name for cv::Mat.
        // Returns empty string as image-stream-format-name that should be checked by caller.
    }

    oFmt.m_ui32Width = oMat.cols;
    oFmt.m_ui32Height = oMat.rows;
    oFmt.m_szMaxByteSize = static_cast<size_t>(oMat.step) * oFmt.m_ui32Height;

    return oFmt;
}

} // end namespace opencvtb.
} // end namespace adtf.
