/**
 *
 * @file
 * Copyright 2024 Digitalwerk GmbH.
 *
 *     This Source Code Form is subject to the terms of the Mozilla
 *     Public License, v. 2.0. If a copy of the MPL was not distributed
 *     with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * If it is not possible or desirable to put the notice in a particular file, then
 * You may include the notice in a location (such as a LICENSE file in a
 * relevant directory) where a recipient would be likely to look for such a notice.
 *
 * You may add additional accurate notices of copyright ownership.
 */

#include "camera_source.hpp"
#include "dnn_filter.hpp"
#include "image_source.hpp"
#include "resize_filter.hpp"
#include "image_converter_filter.hpp"
#include "bayer_filter.hpp"
#include "text_filter.hpp"
#include "face_detector.hpp"

ADTF_PLUGIN_VERSION(
    "OpenCV Components Plugin",
    dw_opencv_components_plugin,
    VERSION_MAJOR,
    VERSION_MINOR,
    VERSION_PATCH,
    cBayerPattern,
    cOpenCVCameraSource,
    cDNNOpenCVFilter,
    cDNNOpenCVPlotFilter,
    cOpenCVImagesSource,
    cResizeFilter,
    cImageConverterFilter,
    cTextFilter,
    cTriggerCounter,
    cFaceDetectorFilter
)
