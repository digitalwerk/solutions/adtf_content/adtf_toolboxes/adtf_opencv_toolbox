/**
 *
 * @file
 * Copyright 2024 Digitalwerk GmbH.
 *
 *     This Source Code Form is subject to the terms of the Mozilla
 *     Public License, v. 2.0. If a copy of the MPL was not distributed
 *     with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * If it is not possible or desirable to put the notice in a particular file, then
 * You may include the notice in a location (such as a LICENSE file in a
 * relevant directory) where a recipient would be likely to look for such a notice.
 *
 * You may add additional accurate notices of copyright ownership.
 */

#include <adtffiltersdk/adtf_filtersdk.h>
#include <adtfsystemsdk/adtf_systemsdk.h>

#include <opencv_base_filter/opencv_samplebuffer.h>
#include <opencv_base_filter/opencv_base_filter.h>

#include <opencv2/opencv.hpp>
#include <opencv2/dnn.hpp>

#include <easy/profiler.h>
#include <cmath>

using namespace adtf::util;
using namespace adtf::ucom;
using namespace adtf::base;
using namespace adtf::streaming;
using namespace adtf::filter;
using namespace adtf::system;
using namespace adtf::mediadescription;

using namespace cv::dnn;
using namespace cv;

using namespace adtf::opencvtb;

class cDNNOpenCVFilter : public cOpenCVBaseFilter
{
public: 
    ADTF_CLASS_ID_NAME(cDNNOpenCVFilter,
        "opencv_dnn.filter.dw.cid",
        "OpenCV DNN Filter");

public:
    property_variable<cFilename> m_strModule;
    property_variable<cFilename> m_strConfig;

    property_variable<tFloat32> m_fBlobScale = 1.0;
    property_variable<tFloat32> m_fBlobMeanRed = 0.0;
    property_variable<tFloat32> m_fBlobMeanGreen = 0.0;
    property_variable<tFloat32> m_fBlobMeanBlue = 0.0;

    property_variable<tInt32> m_fBlobWidth = 224;
    property_variable<tInt32> m_fBlobHeight = 224;

    Net m_oDnnNet;
    std::vector<std::string> m_vecOutputLayerNames;

    property_variable<tInt32> m_nBackend = 0;
    property_variable<tInt32> m_nTarget = 0;

public:
    
    cDNNOpenCVFilter()
    {
        m_strModule.SetDescription("Binary file contains trained weights.");
        m_strModule.SetFilenameExtensionFilter("Caffe (*.caffemodel), TensorFlow (*.pb), Torch (*.t7 *.net), Darknet (*.weights), DLDT (*.bin), ONNX (*.onnx)");
        RegisterPropertyVariable("module", m_strModule);
        m_strConfig.SetDescription("Text file contains network configuration.");
        m_strConfig.SetFilenameExtensionFilter("Caffe (*.prototxt), TensorFlow (*.pbtxt), Darknet (*.cfg), DLDT (*.xml )");
        RegisterPropertyVariable("config", m_strConfig);
        
        m_fBlobScale.SetDescription("Multiplier for image values.");
        RegisterPropertyVariable("blob_scale", m_fBlobScale);
        m_fBlobMeanRed.SetDescription("Red value of 4-dimensional blob from image.");
        RegisterPropertyVariable("blob_mean_red", m_fBlobMeanRed);
        m_fBlobMeanGreen.SetDescription("Green value of 4-dimensional blob from image.");
        RegisterPropertyVariable("blob_mean_green", m_fBlobMeanGreen);
        m_fBlobMeanBlue.SetDescription("Blue value of 4-dimensional blob from image.");
        RegisterPropertyVariable("blob_mean_blue", m_fBlobMeanBlue);
        m_fBlobWidth.SetDescription("Width value of 4-dimensional blob from image.");
        RegisterPropertyVariable("blob_input_width", m_fBlobWidth);
        m_fBlobHeight.SetDescription("Height value of 4-dimensional blob from image.");
        RegisterPropertyVariable("blob_input_height", m_fBlobHeight);

        m_nBackend.SetDescription("Enum of computation backends supported by layers.");
        m_nBackend.SetValueList({
            {cv::dnn::DNN_BACKEND_DEFAULT, "DNN_BACKEND_DEFAULT"},
            {cv::dnn::DNN_BACKEND_HALIDE, "DNN_BACKEND_HALIDE"},
            {cv::dnn::DNN_BACKEND_INFERENCE_ENGINE, "DNN_BACKEND_INFERENCE_ENGINE"},
            {cv::dnn::DNN_BACKEND_OPENCV, "DNN_BACKEND_OPENCV"},
            {cv::dnn::DNN_BACKEND_VKCOM, "DNN_BACKEND_VKCOM"},
            {cv::dnn::DNN_BACKEND_CUDA, "DNN_BACKEND_CUDA"},
            });
        RegisterPropertyVariable("backend", m_nBackend);

        m_nTarget.SetDescription("Enum of target devices for computations.");
        m_nTarget.SetValueList({
            {cv::dnn::DNN_TARGET_CPU, "DNN_TARGET_CPU"},
            {cv::dnn::DNN_TARGET_OPENCL, "DNN_TARGET_OPENCL"},
            {cv::dnn::DNN_TARGET_OPENCL_FP16, "DNN_TARGET_OPENCL_FP16"},
            {cv::dnn::DNN_TARGET_MYRIAD, "DNN_TARGET_MYRIAD"},
            {cv::dnn::DNN_TARGET_VULKAN, "DNN_TARGET_VULKAN"},
            {cv::dnn::DNN_TARGET_FPGA, "DNN_TARGET_FPGA"},
            {cv::dnn::DNN_TARGET_CUDA, "DNN_TARGET_CUDA"},
            {cv::dnn::DNN_TARGET_CUDA_FP16, "DNN_TARGET_CUDA_FP16"},
            });
        RegisterPropertyVariable("target", m_nTarget);

        SetDescription("Use this filter to create and manipulate comprehensive artificial neural networks.");
        SetHelpLink("$(ADTF_OPENCV_TOOLBOX_DIR)/doc/html/opencv_components_plugin_opencv_dnn_filter.html");
    }
    
    ~cDNNOpenCVFilter() override
    {
        
    }

    tResult OnStagePreConnect() override
    {
        if (!cFileSystem::Exists(m_strConfig))
        {
            RETURN_ERROR_DESC(ERR_NOT_FOUND, "Could not find dnn config at %s", m_strConfig->GetPtr());
        }
        if (!cFileSystem::Exists(m_strModule))
        {
            RETURN_ERROR_DESC(ERR_NOT_FOUND, "Could not find dnn module at %s", m_strModule->GetPtr());
        }
        
        LOG_INFO("Load DNN Config %s", m_strConfig->GetPtr());
        LOG_INFO("Load DNN Module %s", m_strModule->GetPtr());

        try
        {
            m_oDnnNet = readNet(m_strConfig->GetPtr(), m_strModule->GetPtr());
        }
        catch (const std::exception& oException)
        {
            LOG_ERROR("Failed to created DNN Net %s", oException.what());
            RETURN_ERROR_DESC(ERR_FAILED, "Failed to created DNN Net %s", oException.what());
        }
        if (m_oDnnNet.empty())
        {
            RETURN_ERROR_DESC(ERR_NOT_READY, "Error while creating Dnn net");
        }

        m_oDnnNet.setPreferableBackend(m_nBackend);
        m_oDnnNet.setPreferableTarget(m_nTarget);
        
        m_vecOutputLayerNames.clear();

        std::vector<int> unconnectedLayers = m_oDnnNet.getUnconnectedOutLayers();
        std::vector<String> allLayerNames = m_oDnnNet.getLayerNames();
        m_vecOutputLayerNames.resize(unconnectedLayers.size());
        
        for (size_t i = 0; i < unconnectedLayers.size(); ++i)
        {
            m_vecOutputLayerNames.push_back(allLayerNames[unconnectedLayers[i] - 1]);
        }

        RETURN_NOERROR;
    }

    tResult ProcessMat(::cv::Mat const& oSrcMat, ::adtf::base::tNanoSeconds oTimestamp, ::std::string oSrcFormatName = "") override
    {
        EASY_FUNCTION()
        if (!m_oDnnNet.empty())
        {
            Mat oBlob = blobFromImage(oSrcMat,
                1.0/255.0,
                Size(m_fBlobWidth, m_fBlobHeight),
                Scalar(m_fBlobMeanRed, m_fBlobMeanGreen, m_fBlobMeanBlue),
                true,
                false);

            m_oDnnNet.setInput(oBlob);
            if (m_oDnnNet.getLayer(0)->outputNameToIndex("im_info") != -1)  // Faster-RCNN or R-FCN
            {
                Mat imInfo = (Mat_<float>(1, 3) << m_fBlobHeight, m_fBlobWidth, 1.6f);
                m_oDnnNet.setInput(imInfo, "im_info");
            }

            auto const & outLayers = m_oDnnNet.getUnconnectedOutLayers();
            auto const & layerNames = m_oDnnNet.getLayerNames();

            std::string outputName;
            if (outLayers.size() > 0)
            {
                int const index = outLayers[0] - 1;
                if ((index >= 0) && (index < int(layerNames.size())))
                {
                    outputName = layerNames[index];
                }
            }

            // From CV docs: If outputName is empty, runs forward pass for the whole network.
            std::vector<Mat> outs;
            m_oDnnNet.forward(outs, outputName);

            if (outs.size() > 0)
            {
                cv::Mat oDstMat = outs[0];

                if (oDstMat.data == oSrcMat.data)
                {
                    RETURN_ERROR_DESC(ERR_INVALID_STATE, "oDstMat.data == oSrcMat.data in DnnFilter");
                }

                RETURN_IF_FAILED(WriteMat(m_pOutput, oDstMat, oTimestamp, oSrcFormatName));
            }
        }

        RETURN_NOERROR;
    }
    
};

class cDNNOpenCVPlotFilter : public cOpenCVBaseFilter
{
public:
    ADTF_CLASS_ID_NAME(cDNNOpenCVPlotFilter,
        "opencv_dnn_plot.filter.dw.cid",
        "OpenCV DNN Plotter");

public:

    struct tAnnotation
    {
        char name[64];
        float confidence;
        uint32_t x;
        uint32_t y;
        uint32_t width;
        uint32_t height;
    };

    struct tAnnotationFinding
    {
        int p1[2]; // x1,y1
        int p2[2]; // x2,y2
        float confidence;
        size_t class_id;
        std::string name;

        int w() const { return p2[0] - p1[0]; }
        int h() const { return p2[1] - p1[1]; }
    };

    cDNNOpenCVPlotFilter()
    {
        SetDescription("Use this filter to plot simple DNN Results.");
        SetHelpLink("$(ADTF_OPENCV_TOOLBOX_DIR)/doc/html/opencv_components_plugin_opencv_dnn_plotter.html");
        object_ptr<IStreamType> pImageStreamType = make_object_ptr<cStreamType>(stream_meta_type_image());
        
        SetDescription("dnn", "Incoming DDN feed.");
        m_pDNNPin = CreateInputPin("dnn", pImageStreamType, false);
        m_pDNNPin->SetAcceptTypeCallback([this](const auto& pStreamType) -> tResult
            {
                RETURN_IF_FAILED(get_stream_type_image_format(m_dnnFormat, *pStreamType.Get()));
                RETURN_NOERROR;
            });

        object_ptr<IStreamType> pStreamType = make_object_ptr<cStreamType>(stream_meta_type_anonymous());
        SetDescription("classes", "Detected classes.");
        m_pClassConfidencePin = CreateOutputPin("classes", pStreamType);

        auto oAnnotation = structure<tAnnotation>("image_annotation")
            .Add("name", &tAnnotation::name)
            .Add("confidence", &tAnnotation::confidence)
            .Add("x", &tAnnotation::x)
            .Add("y", &tAnnotation::y)
            .Add("width", &tAnnotation::width)
            .Add("height", &tAnnotation::height);

        m_pWriteImageAnnotation = CreateOutputPin("annotation", oAnnotation);
        SetDescription("annotation", "Image annotations wiht classname and position");

        tStreamImageFormat oCropFormat;
        oCropFormat.m_strFormatName = "R(8)G(8)B(8)";
        oCropFormat.m_ui32Width = 512;
        oCropFormat.m_ui32Height = 512;
        oCropFormat.m_szMaxByteSize = 512 * 512 * 3;

        pImageStreamType = make_object_ptr<cStreamType>(stream_meta_type_image());
        set_stream_type_image_format(*pImageStreamType.Get(), oCropFormat);

        m_pCroppedClass1 = CreateOutputPin("cropped1", pImageStreamType);
        SetDescription("cropped1", "Croppe image from the first class found");

        m_pClassName1 = CreateOutputPin<pin_writer<std::string>>("classname1", stream_type_string<std::string>());
        SetDescription("classname1", "Provides a string with class name");
        
        m_pClassConfidence1 = CreateOutputPin("classconfidence1", stream_type_plain<double>());
        SetDescription("classconfidence1", "Provides the confidence of first class");
    }

    ~cDNNOpenCVPlotFilter() override
    {

    }

    tResult ProcessMat(::cv::Mat const& oSrcMat, ::adtf::base::tNanoSeconds oTimestamp, ::std::string oSrcFormatName = "") override
    {
        EASY_FUNCTION()

        {
            object_ptr<const ISample> pSample;
            m_pDNNPin->GetLastSample(pSample);
            if (pSample)
            {
                m_pDNNData = pSample;
            }
        }

        cv::Mat oDstMat = oSrcMat;

        std::vector<cv::Point> lstCenterPoints;

        if (m_pDNNData)
        {
            std::vector<float> lstClasses;
            std::vector<tAnnotationFinding> lstAnnotations;

            object_ptr_shared_locked<const ISampleBuffer> pDnnBuffer;

            cv::Mat dnnMat;

            if (IS_OK(m_pDNNData->Lock(pDnnBuffer)))
            {
                dnnMat = Mat(
                    m_dnnFormat.m_ui32Height,
                    m_dnnFormat.m_ui32Width,
                    m_nMatType.value(),
                    const_cast<void*>(pDnnBuffer->GetPtr()),
                    pDnnBuffer->GetSize() / m_dnnFormat.m_ui32Height);

                if (pDnnBuffer->GetSize() != m_dnnFormat.m_szMaxByteSize)
                {
                    THROW_ERROR_DESC(ERR_NOT_SUPPORTED, "received dnn sample size miss match %" PRIu64 " != %" PRIu64, 
                                     static_cast<uint64_t>(pDnnBuffer->GetSize()), 
                                     static_cast<uint64_t>(m_dnnFormat.m_szMaxByteSize));
                }
            }

            auto data = reinterpret_cast<float const*>(dnnMat.datastart);
            auto const w = size_t(dnnMat.cols);

            for (size_t i = 0; i < dnnMat.total(); i += w)
            {
                auto confidence = 0.f;
                size_t classid = 0;

                for (size_t j = 5; j < w; j++)
                {
                    const float c = data[i + j];

                    if (confidence < c)
                    {
                        //log.info("Confidence: " + c + " " + j + " " + root.classes[j]);
                        confidence = c;
                        classid = j - 5;
                    }
                }

                const auto center_x = static_cast<int>(data[i + 0] * oSrcMat.cols);
                const auto center_y = static_cast<int>(data[i + 1] * oSrcMat.rows);
                const auto width = static_cast<int>(data[i + 2] * oSrcMat.cols);
                const auto height = static_cast<int>(data[i + 3] * oSrcMat.rows);

                // Remove duplicate found objects
                const cv::Point oP1(center_x, center_y);
                bool bFound = false;

                for (auto oP2 : lstCenterPoints)
                {
                    const double fCross = std::abs((oP2.x - oP1.x) * (oP2.x - oP1.x) + (oP2.y - oP1.y) * (oP2.y - oP1.y));

                    if (std::sqrt(fCross) < 75)
                    {
                        bFound = true;
                    }
                }

                if (!bFound && confidence > 0.05)
                {
                    lstCenterPoints.push_back(oP1);

                    tAnnotationFinding oAnnotation;
                    oAnnotation.p1[0] = center_x - width / 2;
                    oAnnotation.p1[1] = center_y - height / 2;
                    oAnnotation.p2[0] = center_x + width / 2;
                    oAnnotation.p2[1] = center_y + height / 2;
                    oAnnotation.confidence = confidence;
                    oAnnotation.class_id = classid;

                    if (classid < m_lstClasses.size())
                    {
                        oAnnotation.name = m_lstClasses[classid];
                    }
                    else
                    {
                        LOG_WARNING("Class with id '%" PRIu64 "' not found", static_cast< uint64_t >(classid));
                    }

                    lstAnnotations.push_back(oAnnotation);
                }
            }

            if (lstAnnotations.size() > 0)
            {
                auto& oAnnotation = lstAnnotations[0];
                {
                    EASY_BLOCK("Crop Image 1")

                    try
                    {
                        cv::Mat oCropMat = oSrcMat(Range(oAnnotation.p1[1], oAnnotation.p2[1]), Range(oAnnotation.p1[0], oAnnotation.p2[0]));
                        cv::Mat oResizeMat;
                        cv::resize(oCropMat, oResizeMat, Size(512, 512), INTER_LINEAR);
                        if (oResizeMat.rows > 10 && oResizeMat.cols > 10)
                        {
                            RETURN_IF_FAILED(WriteMat(m_pCroppedClass1, oResizeMat, oTimestamp, oSrcFormatName));
                        }
                    }
                    catch (...)
                    {

                    }
                }

                {
                    EASY_BLOCK("Write Image Annotation")
                    output_sample_data<tAnnotation> oSample(0);

                    oSample->confidence = oAnnotation.confidence;
                    oSample->x = oAnnotation.p1[0];
                    oSample->y = oAnnotation.p1[1];
                    oSample->width = oAnnotation.p2[0] - oAnnotation.p1[0];
                    oSample->height = oAnnotation.p2[1] - oAnnotation.p1[1];

                    memset(oSample->name, 0, 64);

                    oAnnotation.name.copy(oSample->name, 64);
                    m_pWriteImageAnnotation->Write(oSample.Release());
                }

                {
                    EASY_BLOCK("Write Object");
                    m_pClassName1->Write(oTimestamp, oAnnotation.name);
                }

                {
                    EASY_BLOCK("Write Confidence");
                    output_sample_data<double> oConfidenceSample(0);
                    oConfidenceSample = oAnnotation.confidence;
                    m_pClassConfidence1->Write(oConfidenceSample.Release());
                }
            }
            else
            {
                output_sample_data<tAnnotation> oSample(0);

                oSample->confidence = 0.f;
                oSample->x = 0;
                oSample->y = 0;
                oSample->width = 0;
                oSample->height = 0;

                memset(oSample->name, 0, 64);

                m_pWriteImageAnnotation->Write(oSample.Release());
            }

            {
                EASY_BLOCK("Draw Objects");

                // Is there something to draw?
                if (lstAnnotations.size() > 0)
                {
                    // Clone before drawing, if neccessary.
                    if (oDstMat.data == oSrcMat.data)
                    {
                        oDstMat = oSrcMat.clone();
                    }

                    // Drawing.
                    for (auto oAnnotation : lstAnnotations)
                    {
                        DrawPred(oAnnotation.name, oAnnotation.confidence,
                            oAnnotation.p1[0],
                            oAnnotation.p1[1],
                            oAnnotation.p2[0],
                            oAnnotation.p2[1],
                            oDstMat);

                        lstClasses.push_back(static_cast<float>(oAnnotation.class_id));
                        lstClasses.push_back(oAnnotation.confidence);
                    }
                }
            }

            object_ptr<ISample> pClassesSample;
            if (IS_OK(alloc_sample(pClassesSample, oTimestamp)))
            {
                object_ptr_locked<ISampleBuffer> pClassesBuffer;
                if (IS_OK(pClassesSample->WriteLock(pClassesBuffer, lstClasses.size() * sizeof(tFloat))))
                {
                    cMemoryBlock::MemCopy(pClassesBuffer->GetPtr(), lstClasses.data(), lstClasses.size() * sizeof(tFloat));
                }
                m_pClassConfidencePin->Write(pClassesSample);
            }

            RETURN_IF_FAILED(WriteMat(m_pOutput, oDstMat, oTimestamp, oSrcFormatName));
        }

        RETURN_NOERROR;
    }

    void DrawPred(std::string name, float conf, int left, int top, int right, int bottom, Mat& frame)
    {
        rectangle(frame, Point(left, top), Point(right, bottom), Scalar(0, 255, 0), 2);

        std::string label = name + ": " +format("%.2f", conf);

        int baseLine;
        const Size labelSize = getTextSize(label, FONT_HERSHEY_SIMPLEX, 0.5, 1, &baseLine);

        top = max(top, labelSize.height);
        rectangle(frame, Point(left, top - labelSize.height),
            Point(left + labelSize.width, top + baseLine), Scalar::all(255), FILLED);
        putText(frame, label, Point(left, top), FONT_HERSHEY_SIMPLEX, 0.5, Scalar());
    }

private:
    cPinReader* m_pDNNPin;
    cPinWriter* m_pClassConfidencePin;
    cPinWriter* m_pWriteImageAnnotation;
    cPinWriter* m_pCroppedClass1;

    pin_writer<std::string>* m_pClassName1;
    cPinWriter* m_pClassConfidence1;

    tStreamImageFormat m_dnnFormat;
    object_ptr<const ISample> m_pDNNData;

    std::vector<std::string> m_lstClasses = { "person","bicycle","car","motorbike","aeroplane","bus","train","truck","boat","traffic light","fire hydrant","stop sign","parking meter","bench","bird","cat","dog","horse","sheep","cow","elephant","bear","zebra","giraffe","backpack","umbrella","handbag","tie","suitcase","frisbee","skis","snowboard","sports ball","kite","baseball bat","baseball glove","skateboard","surfboard","tennis racket","bottle","wine glass","cup","fork","knife","spoon","bowl","banana","apple","sandwich","orange","broccoli","carrot","hot dog","pizza","donut","cake","chair","sofa","pottedplant","bed","diningtable","toilet","tvmonitor","laptop","mouse","remote","keyboard","cell phone","microwave","oven","toaster","sink","refrigerator","book","clock","vase","scissors","teddy bear","hair drier","toothbrush" };
};
