/**
 *
 * @file
 * Copyright 2024 Digitalwerk GmbH.
 *
 *     This Source Code Form is subject to the terms of the Mozilla
 *     Public License, v. 2.0. If a copy of the MPL was not distributed
 *     with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * If it is not possible or desirable to put the notice in a particular file, then
 * You may include the notice in a location (such as a LICENSE file in a
 * relevant directory) where a recipient would be likely to look for such a notice.
 *
 * You may add additional accurate notices of copyright ownership.
 */

#include <adtffiltersdk/adtf_filtersdk.h>
#include <adtfsystemsdk/adtf_systemsdk.h>

#include <opencv_base_filter/opencv_base_filter.h>

#include <opencv2/opencv.hpp>
#include <opencv2/dnn.hpp>

using namespace adtf::util;
using namespace adtf::ucom;
using namespace adtf::base;
using namespace adtf::streaming;
using namespace adtf::filter;
using namespace adtf::system;

using namespace cv::dnn;
using namespace cv;

using namespace adtf::opencvtb;

class cBayerPattern : public cOpenCVBaseFilter
{
public:
    ADTF_CLASS_ID_NAME(cBayerPattern,
        "opencv_bayer_pattern.filter.dw.cid",
        "OpenCV Bayer Converter");

public:
    cBayerPattern()
    {
        SetDescription("BayerFilter converts raw camera sensor greyscale 8bit (GBRG) image data to BGR 24 images.");
        SetHelpLink("$(ADTF_OPENCV_TOOLBOX_DIR)/doc/html/opencv_components_plugin_opencv_bayer_converter.html");
    }

    tStreamImageFormat ConvertImageFormat(const tStreamImageFormat & oInputFormat) override
    {
        if (oInputFormat.m_strFormatName != ADTF_IMAGE_FORMAT(GREYSCALE_8))
        {
            THROW_ERROR_DESC(ERR_INVALID_ARG,"OpenCV BayerPattern expects input format GREYSCALE_8");
        }

        if (oInputFormat.m_ui32Width < 4)
        {
            THROW_ERROR_DESC(ERR_INVALID_ARG, "OpenCV BayerPattern expects minimum width 4 and increments of 2 (4,6,8,...)");
        }
        else
        {
            if (oInputFormat.m_ui32Width % 2 != 0)
            {
                THROW_ERROR_DESC(ERR_INVALID_ARG, "OpenCV BayerPattern expects minimum width 4 and increments of 2 (4,6,8,...)");
            }
        }

        if (oInputFormat.m_ui32Height < 4)
        {
            THROW_ERROR_DESC(ERR_INVALID_ARG, "OpenCV BayerPattern expects minimum height 4 and increments of 2 (4,6,8,...)");
        }
        else
        {
            if (oInputFormat.m_ui32Height % 2 != 0)
            {
                THROW_ERROR_DESC(ERR_INVALID_ARG, "OpenCV BayerPattern expects minimum height 4 and increments of 2 (4,6,8,...)");
            }
        }

        tStreamImageFormat oOutFormat;
        oOutFormat.m_strFormatName = ADTF_IMAGE_FORMAT(BGR_24);
        oOutFormat.m_ui32Width = oInputFormat.m_ui32Width;
        oOutFormat.m_ui32Height = oInputFormat.m_ui32Height;
        oOutFormat.m_szMaxByteSize = size_t(oOutFormat.m_ui32Width) * oOutFormat.m_ui32Height * 3u;

        return oOutFormat;
    }

    tResult ProcessMat(::cv::Mat const& oSrcMat, ::adtf::base::tNanoSeconds oTimestamp, ::std::string /* oSrcFormatName = "" */) override
    {
        cv::Mat oDstMat;
        cv::demosaicing(oSrcMat, oDstMat, cv::COLOR_BayerBG2BGR);
        RETURN_IF_FAILED(WriteMat(m_pOutput, oDstMat, oTimestamp, ADTF_IMAGE_FORMAT(BGR_24)));
        RETURN_NOERROR;
    }

};
