/**
 *
 * @file
 * Copyright 2024 Digitalwerk GmbH.
 *
 *     This Source Code Form is subject to the terms of the Mozilla
 *     Public License, v. 2.0. If a copy of the MPL was not distributed
 *     with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * If it is not possible or desirable to put the notice in a particular file, then
 * You may include the notice in a location (such as a LICENSE file in a
 * relevant directory) where a recipient would be likely to look for such a notice.
 *
 * You may add additional accurate notices of copyright ownership.
 */

#pragma once

#include <adtffiltersdk/adtf_filtersdk.h>
#include <adtfsystemsdk/adtf_systemsdk.h>

#include <opencv_base_filter/opencv_samplebuffer.h>
#include <opencv_base_filter/opencv_base_filter.h>

#include <opencv2/opencv.hpp>
#include <opencv2/dnn.hpp>
#include <easy/profiler.h>

using namespace adtf::util;
using namespace adtf::ucom;
using namespace adtf::base;
using namespace adtf::streaming;
using namespace adtf::filter;
using namespace adtf::system;

using namespace cv::dnn;
using namespace cv;

using namespace adtf::opencvtb;

class cOpenCVCameraSource : public adtf::filter::cFilter
{
public:
    ADTF_CLASS_ID_NAME(cOpenCVCameraSource,
        "opencv_capture_camera.filter.dw.cid",
        "OpenCV Capture Camera");

    ADTF_CLASS_DEPENDENCIES(REQUIRE_INTERFACE(adtf::services::IReferenceClock),
                            REQUIRE_INTERFACE(adtf::services::IKernel));

public:
    property_variable<tInt32> m_nCameraID = 0;

    VideoCapture m_oCamera;

    cPinWriter* m_pOutput;
    tStreamImageFormat m_sCurrentFormat;

    property_variable<uint32_t> m_nWidth = 0;
    property_variable<uint32_t> m_nHeight = 0;
    property_variable<uint32_t> m_nFrames = 0;

public:

    cOpenCVCameraSource()
    {
        SetHelpLink("$(ADTF_OPENCV_TOOLBOX_DIR)/doc/html/opencv_components_plugin_opencv_capture_camera.html");
        SetDescription("Use this Streaming Source to capture images from cameras.");

        m_nCameraID.SetDescription("Device ID to connect to.");
        RegisterPropertyVariable("cameraId", m_nCameraID);
        
        SetDescription("image", "Captured image.");
        m_pOutput = CreateOutputPin("image");


        CreateRunner("capture", [this](adtf::base::tNanoSeconds oTimestamp)
        {
            return CaptureImage(oTimestamp);
        });
        SetDescription("capture", "Call capture on the opencv VideoCapture object and transmit the image sample");

        m_nWidth.SetDescription("Width of the frames in the video stream (0 set nothing in opencv)");
        RegisterPropertyVariable("width", m_nWidth);

        m_nHeight.SetDescription("Height of the frames in the video stream (0 set nothing in opencv)");
        RegisterPropertyVariable("height", m_nHeight);

        m_nFrames.SetDescription("Frame rate. (0 set nothing in opencv)");
        RegisterPropertyVariable("frames", m_nFrames);
    }

    ~cOpenCVCameraSource()
    {

    }

    tResult Init(tInitStage eStage) override
    {
        if(eStage == tInitStage::StageGraphReady)
        {
#ifdef _WIN32
            m_oCamera.open(*m_nCameraID + cv::CAP_DSHOW);
#else
            m_oCamera.open(*m_nCameraID + cv::CAP_V4L2);
#endif
            if (!m_oCamera.isOpened())
            {
                RETURN_ERROR_DESC(ERR_DEVICE_NOT_READY, "Unable to open camera");
            }
            int fourcc = cv::VideoWriter::fourcc('M', 'J', 'P', 'G');
            m_oCamera.set(cv::CAP_PROP_FOURCC, fourcc);
            if (m_nWidth > 0 && m_nHeight > 0)
            {
                if ((!m_oCamera.set(cv::CAP_PROP_FRAME_WIDTH, m_nWidth)) ||
                    (!m_oCamera.set(cv::CAP_PROP_FRAME_HEIGHT, m_nHeight)))
                {
                    RETURN_ERROR_DESC(ERR_DEVICE_NOT_READY, "Fail to set capture property width x height");
                }
            }

            if(m_nFrames > 0)
            {
                if (!m_oCamera.set(cv::CAP_PROP_FPS, m_nFrames))
                {
                    RETURN_ERROR_DESC(ERR_DEVICE_NOT_READY, "Fail to set capture property frame");
                }
            }
        }
        RETURN_NOERROR;
    }

    tResult Shutdown(tInitStage eStage) override
    {
        if (eStage == tInitStage::StageGraphReady)
        {
            m_oCamera.release();
        }
        RETURN_NOERROR;
    }

    tResult CaptureImage(adtf::base::tNanoSeconds oTimestamp)
    {
        Mat oMatImage;
        {
            EASY_BLOCK("Capture Image");
            m_oCamera >> oMatImage;
        }

        if (oMatImage.empty())
        {
            LOG_ERROR("Captured image is empty");
            RETURN_NOERROR;
        }

        auto const oDstFormat = createCompatibleImageStreamFormat(oMatImage);
        if (oDstFormat.m_strFormatName == "")
        {
            LOG_ERROR("Unsupported empty stream-format-name for cv::Mat type %" PRIu32, static_cast<uint32_t>(oMatImage.type()));
            RETURN_NOERROR;
        }

        if (!isSameFormat(m_sCurrentFormat, oDstFormat))
        {
            m_sCurrentFormat = oDstFormat;
            object_ptr<IStreamType> pImageStreamType = make_object_ptr<cStreamType>(stream_meta_type_image());
            RETURN_IF_FAILED(set_stream_type_image_format(*pImageStreamType.Get(), oDstFormat));
            RETURN_IF_FAILED(m_pOutput->ChangeType(pImageStreamType));
        }

        object_ptr<ISample> pSample;
        RETURN_IF_FAILED(createOpenCVSample(pSample, oMatImage, oTimestamp));
        RETURN_IF_FAILED(m_pOutput->Write(object_ptr<const ISample>(pSample)));
        RETURN_NOERROR;
    }

};
