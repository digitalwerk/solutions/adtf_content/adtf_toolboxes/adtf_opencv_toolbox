/**
 *
 * @file
 * Copyright 2024 Digitalwerk GmbH.
 *
 *     This Source Code Form is subject to the terms of the Mozilla
 *     Public License, v. 2.0. If a copy of the MPL was not distributed
 *     with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * If it is not possible or desirable to put the notice in a particular file, then
 * You may include the notice in a location (such as a LICENSE file in a
 * relevant directory) where a recipient would be likely to look for such a notice.
 *
 * You may add additional accurate notices of copyright ownership.
 */

#include <adtffiltersdk/adtf_filtersdk.h>
#include <adtfsystemsdk/adtf_systemsdk.h>

#include <opencv_base_filter/opencv_base_filter.h>

#include <opencv2/opencv.hpp>

using namespace adtf::util;
using namespace adtf::ucom;
using namespace adtf::base;
using namespace adtf::streaming;
using namespace adtf::filter;
using namespace adtf::system;
using namespace adtf::opencvtb;

/*
 * cImageConverterFilter
 * 
 */
class cImageConverterFilter final : public cOpenCVBaseFilter
{
public:
    ADTF_CLASS_ID_NAME(cImageConverterFilter,
        "opencv_image_converter.filter.dw.cid",
        "OpenCV Image Converter");

private:
    // [1.] Output format converter
    //! Enum specifying supported output formats.
    enum class eOutputFormat {
        Original = 0,
        RGB_888,
        BGR_888,
        GREYSCALE_8
    };
    //! Controls output format converter.
    property_variable<eOutputFormat> m_iOutputFormat = eOutputFormat::Original;

    // [2.] Crop
    //! Crop image left in [px], must be positive number or is disabled.
    property_variable<int32_t> m_iCropLeft = 0;
    //! Crop image top in [px], must be positive number or is disabled.
    property_variable<int32_t> m_iCropTop = 0;
    //! Crop image right in [px], must be positive number or is disabled.
    property_variable<int32_t> m_iCropRight = 0;
    //! Crop image bottom in [px], must be positive number or is disabled.
    property_variable<int32_t> m_iCropBottom = 0;

    // [3.] Flip
    //! Enum to control image flipping/mirroring.
    enum class eFlip { Disabled = 0, Horizontal, Vertical, Both };
    //! Control image flipping/mirroring.    
    property_variable<eFlip> m_iFlip = eFlip::Disabled;

    // [4.] Rotate
    //! Enum to control image rotation in 90 degree steps.
    enum class eRotate { Disabled = 0, CW_90, CW_180, CW_270 };
    //! Control image rotation in 90 degree steps.    
    property_variable<eRotate> m_iRotate = eRotate::Disabled;

    // [5.] Resize
    //! Enum for resize filter modes.
    enum class eResizeFilter 
    { 
        //! Nearest neighbor interpolation.
        Nearest = 0,
        //! (Bi)Linear interpolation.
        Linear, 
        //! (Bi)Cubic interpolation.
        Cubic,
        //! Lanczos4 interpolation over 8x8 neighborhood.
        Lanczos,
        //! Bit exact (bi)linear interpolation
        LinearExact,
        //! Bit exact nearest neighbor interpolation. This will produce same
        //! results as the nearest neighbor method in PIL, scikit or Matlab.
        NearestExact,
        //! Resampling using pixel area relation. It may be a preferred method
        //! for image decimation, as it gives Moire'-free results.
        //! It is similar to the Nearest method.
        Area,
    };
    //! Resize filter mode.
    property_variable<eResizeFilter> m_iResizeFilterMode = eResizeFilter::Lanczos;
    //! Resize width in [px], must be positive number or disabled.
    property_variable<int32_t> m_iResizeWidth = 0;
    //! Resize height in [px], must be positive number or disabled.
    property_variable<int32_t> m_iResizeHeight = 0;
    //! Resize aspect ratio mode.
    property_variable<bool> m_bResizeKeepAspectRatio = false;

public:
    cImageConverterFilter()
    {
        SetHelpLink("$(ADTF_OPENCV_TOOLBOX_DIR)/doc/html/opencv_components_plugin_opencv_image_converter.html");
        SetDescription(
            "This filter combines 5 image operation in a fixed order.\n"
            "    1. Output format conversion.\n"
            "    2. Crop image borders.\n"
            "    3. Flipping / Mirroring.\n"
            "    4. Rotate in 90 degree steps.\n"
            "    5. Resize with quality and aspect ratio choices.\n");

        // [1.] Output format converter
        m_iOutputFormat.SetDescription("Desired OpenCV output format.");
        m_iOutputFormat.SetValueList({
            { eOutputFormat::Original, "Original Format" },
            { eOutputFormat::RGB_888, "RGB_888" },
            { eOutputFormat::BGR_888, "BGR_888" },
            { eOutputFormat::GREYSCALE_8, "GREYSCALE_8" } });
        RegisterPropertyVariable("output_format", m_iOutputFormat);

        // [2.] Crop
        m_iCropLeft.SetDescription("Crop image left in pixel.");
        m_iCropTop.SetDescription("Crop image top in pixel.");
        m_iCropRight.SetDescription("Crop image right in pixel.");
        m_iCropBottom.SetDescription("Crop image bottom in pixel.");
        RegisterPropertyVariable("crop/left", m_iCropLeft);
        RegisterPropertyVariable("crop/top", m_iCropTop);
        RegisterPropertyVariable("crop/right", m_iCropRight);
        RegisterPropertyVariable("crop/bottom", m_iCropBottom);

        // [3.] Flip/Mirror
        m_iFlip.SetDescription( "Flip/Mirror image.");
        m_iFlip.SetValueList({
            { eFlip::Disabled, "Disabled" },
            { eFlip::Horizontal, "Horizontal" },
            { eFlip::Vertical, "Vertical" },
            { eFlip::Both, "Both" } });
        RegisterPropertyVariable("flip", m_iFlip);
        
        // [4.] Rotate90
        m_iRotate.SetDescription(
            "Rotate image clock-wise in 90 degree steps.");
        m_iRotate.SetValueList( {
            { eRotate::Disabled, "Disabled" },
            { eRotate::CW_90, "Rotate 90 degrees clock-wise" },
            { eRotate::CW_180, "Rotate 180 degrees clock-wise" },
            { eRotate::CW_270, "Rotate 270 degrees clock-wise" } });
        RegisterPropertyVariable("rotate", m_iRotate);

        // [5.] Resize
        m_iResizeFilterMode.SetDescription(
            "Resize filter quality mode.\n"
            "Disable resizing by setting width and height to zero.\n");
        m_iResizeFilterMode.SetValueList( {   
            { eResizeFilter::Nearest,"Nearest neighbor interpolation" },
            { eResizeFilter::Linear, "Linear interpolation" },
            { eResizeFilter::Cubic,  "Cubic interpolation" },
            { eResizeFilter::Lanczos,"Lanczos 8x8 neighborhood" },
            { eResizeFilter::LinearExact, "Bit exact Linear" },
            { eResizeFilter::NearestExact,"Bit exact Nearest neighbor" },
            { eResizeFilter::Area,  "Area relation, nearest like" } });
        m_iResizeWidth.SetDescription(
            "Desired (max) width in pixel after resizing.\n"
            "Default: 0 - Resizing is disabled ( with keep-aspect = false )\n"
            "         or width is auto fitted ( with keep-aspect = true).\n"
            "Disable resizing by setting (width = 0) and (height = 0).\n");
        m_iResizeHeight.SetDescription(
            "Desired (max) height in pixel after resizing.\n"
            "Default: 0 - Resizing is disabled ( with keep-aspect = false )\n"
            "         or height is auto fitted ( with keep-aspect = true).\n"
            "Disable resizing by setting (width = 0) and (height = 0).\n");
        m_bResizeKeepAspectRatio.SetDescription(
            "Keep original aspect ratio.\n"
            "- If enabled resize-width and resize-height define\n"
            "  a maximum rect the image will be fit into.\n"
            "- If disabled the image will be stretched\n"
            "  to cover the desired resize size.");
        RegisterPropertyVariable("resize/filter_mode", m_iResizeFilterMode);
        RegisterPropertyVariable("resize/width", m_iResizeWidth);
        RegisterPropertyVariable("resize/height", m_iResizeHeight);
        RegisterPropertyVariable("resize/keep_aspect_ratio", m_bResizeKeepAspectRatio);
    }

    tResult ProcessMat(::cv::Mat const& oSrcMat, ::adtf::base::tNanoSeconds oTimestamp, ::std::string oSrcFormatName = "") override
    {
        std::string oDstFormatName = oSrcFormatName;
        cv::Mat oDstMat = oSrcMat;

        // [1.] Output format converter
        // We convert first to prevent yet another possible costly copy at the end.
        if (m_iOutputFormat != eOutputFormat::Original)
        {
            if (m_iOutputFormat == eOutputFormat::RGB_888)
            {
                if (oSrcFormatName == ADTF_IMAGE_FORMAT(BGR_24))
                {
                    cv::Mat oTmpMat;
                    cv::cvtColor(oDstMat, oTmpMat, COLOR_BGR2RGB);
                    oDstMat = oTmpMat;
                    if (oSrcMat.data == oDstMat.data)
                    {
                        THROW_ERROR_DESC(ERR_INVALID_ARG, "Output format converter did not clone().");
                    }
                }
                else if (oSrcFormatName == ADTF_IMAGE_FORMAT(GREYSCALE_8))
                {
                    cv::Mat oTmpMat;
                    cv::cvtColor(oDstMat, oTmpMat, COLOR_GRAY2RGB);
                    oDstMat = oTmpMat;
                    if (oSrcMat.data == oDstMat.data)
                    {
                        THROW_ERROR_DESC(ERR_INVALID_ARG, "Output format converter did not clone().");
                    }
                }
                else if (oSrcFormatName == ADTF_IMAGE_FORMAT(RGB_24))
                {
                    // Nothing todo.
                }
                else
                {
                    THROW_ERROR_DESC(ERR_FAILED, "Unsupported input format %s", oSrcFormatName.c_str());
                }
                oDstFormatName = ADTF_IMAGE_FORMAT(RGB_24);
            }
            else if (m_iOutputFormat == eOutputFormat::BGR_888)
            {
                if (oSrcFormatName == ADTF_IMAGE_FORMAT(RGB_24))
                {
                    cv::Mat oTmpMat;
                    cv::cvtColor(oDstMat, oTmpMat, COLOR_RGB2BGR);
                    oDstMat = oTmpMat;
                    if (oSrcMat.data == oDstMat.data)
                    {
                        THROW_ERROR_DESC(ERR_INVALID_ARG, "Output format converter did not clone().");
                    }
                }
                else if (oSrcFormatName == ADTF_IMAGE_FORMAT(GREYSCALE_8))
                {
                    cv::Mat oTmpMat;
                    cv::cvtColor(oDstMat, oTmpMat, COLOR_GRAY2BGR);
                    oDstMat = oTmpMat;
                    if (oSrcMat.data == oDstMat.data)
                    {
                        THROW_ERROR_DESC(ERR_INVALID_ARG, "Output format converter did not clone().");
                    }
                }
                else if (oSrcFormatName == ADTF_IMAGE_FORMAT(BGR_24))
                {
                    // Nothing todo.
                }
                else
                {
                    THROW_ERROR_DESC(ERR_FAILED, "Unsupported input format %s", oSrcFormatName.c_str());
                }
                oDstFormatName = ADTF_IMAGE_FORMAT(BGR_24);
            }
            else if (m_iOutputFormat == eOutputFormat::GREYSCALE_8)
            {
                if (oSrcFormatName == ADTF_IMAGE_FORMAT(RGB_24))
                {
                    cv::Mat oTmpMat;
                    cv::cvtColor(oDstMat, oTmpMat, COLOR_RGB2GRAY);
                    oDstMat = oTmpMat;
                    if (oSrcMat.data == oDstMat.data)
                    {
                        THROW_ERROR_DESC(ERR_INVALID_ARG, "Output format converter did not clone().");
                    }
                }
                else if (oSrcFormatName == ADTF_IMAGE_FORMAT(BGR_24))
                {
                    cv::Mat oTmpMat;
                    cv::cvtColor(oDstMat, oTmpMat, COLOR_BGR2GRAY);
                    oDstMat = oTmpMat;
                    if (oSrcMat.data == oDstMat.data)
                    {
                        THROW_ERROR_DESC(ERR_INVALID_ARG, "Output format converter did not clone().");
                    }
                }
                else if (oSrcFormatName == ADTF_IMAGE_FORMAT(GREYSCALE_8))
                {
                    // Nothing todo.
                }
                else
                {
                    THROW_ERROR_DESC(ERR_FAILED, "Unsupported input format %s", oSrcFormatName.c_str());
                }
                oDstFormatName = ADTF_IMAGE_FORMAT(GREYSCALE_8);
            }
        }

        // [2.] Crop
        if ((m_iCropLeft >= 0) && (m_iCropTop >= 0) && (m_iCropRight >= 0) && (m_iCropBottom >= 0))
        {
            int32_t const crop_w = m_iCropLeft + m_iCropRight;
            int32_t const crop_h = m_iCropTop + m_iCropBottom;
            if ((crop_w > 0) || (crop_h > 0))
            {
                int32_t const src_w = oSrcMat.cols;
                int32_t const src_h = oSrcMat.rows;
                int32_t const dst_w = src_w - crop_w;
                int32_t const dst_h = src_h - crop_h;
                if ((dst_w > 0) && (dst_h > 0))
                {
                    // Don't clone, we change cv::Mat (view on buffer)
                    // but we dont change the underlying buffer of cv::Mat.

                    // Crop image
                    cv::Rect oCropRect(m_iCropLeft, m_iCropTop, dst_w, dst_h);
                    cv::Mat oCropMat = oDstMat(oCropRect);
                    cv::Mat oTmpMat;
                    oCropMat.copyTo(oTmpMat);
                    oDstMat = oTmpMat;
                }
            }
        }

        // [3.] + [4.] transpose() creates new cv::Mat automatically.
        bool bRotate_CW_90 = false;
        bool bRotate_CW_180 = false;
        bool bRotate_CW_270 = false;
        bool bFlip_H = false;
        bool bFlip_V = false;
        bool bFlip_Both = false;

        switch (m_iRotate)
        {
            case eRotate::CW_90: bRotate_CW_90 = true; break;
            case eRotate::CW_180: bRotate_CW_180 = true; break;
            case eRotate::CW_270: bRotate_CW_270 = true; break;
            default: break;
        }

        switch (m_iFlip)
        {
            case eFlip::Horizontal: bFlip_H = true; break;
            case eFlip::Vertical: bFlip_V = true; break;
            case eFlip::Both: bFlip_Both = true; break;
            default: break;
        }

        bool const bNeedTranspose = bRotate_CW_270 ^ bRotate_CW_90;
        bool const bNeedFlipH = bFlip_Both ^ bFlip_H ^ bRotate_CW_90 ^ bRotate_CW_180;
        bool const bNeedFlipV = bFlip_Both ^ bFlip_V ^ bRotate_CW_180 ^ bRotate_CW_270;

        // [3.] Transpose()
        if (bNeedTranspose)
        {
            cv::Mat oTmpMat;
            cv::transpose(oDstMat, oTmpMat);
            oDstMat = oTmpMat; // Prepare for next filter.
        }

        // [4.] Flip()
        if (bNeedFlipH || bNeedFlipV)
        {
            int32_t oFlag = 0;
            if (bNeedFlipH && bNeedFlipV)
            {
                oFlag = -1; // eFlip::Both
            }
            else
            {
                if (bNeedFlipH)
                {
                    oFlag = 1; // eFlip::Horizontal
                }
                else
                {
                    oFlag = 0; // eFlip::Vertical
                }
            }

            cv::Mat oTmpMat;
            cv::flip(oDstMat, oTmpMat, oFlag);
            oDstMat = oTmpMat; // Prepare for next filter.

            if (oDstMat.data == oSrcMat.data)
            {
                LOG_ERROR("Flip() did not clone().");
            }
        }

        // [5.] Resize
        int32_t const src_w = oDstMat.cols;
        int32_t const src_h = oDstMat.rows;
        cv::Size dst_size = ComputeResizeSize(src_w, src_h);
        int32_t const dst_w = dst_size.width;
        int32_t const dst_h = dst_size.height;
            
        // We need a resize.
        if ((src_w != dst_w) || (src_h != dst_h))
        {
            int32_t oMode = 0;
            switch (m_iResizeFilterMode)
            {
                case eResizeFilter::Nearest: oMode = cv::INTER_NEAREST; break;
                case eResizeFilter::Linear: oMode = cv::INTER_LINEAR; break;
                case eResizeFilter::Cubic: oMode = cv::INTER_CUBIC; break;
                case eResizeFilter::Lanczos: oMode = cv::INTER_LANCZOS4; break;
                case eResizeFilter::LinearExact: oMode = cv::INTER_LINEAR_EXACT; break;
                case eResizeFilter::NearestExact: oMode = cv::INTER_NEAREST_EXACT; break;
                case eResizeFilter::Area: oMode = cv::INTER_AREA; break;
                default:
                {
                    THROW_ERROR_DESC(ERR_INVALID_ARG, "Invalid resize filter mode argument");
                }
            }

            cv::Mat oTmpMat;
            cv::resize(oDstMat, oTmpMat, dst_size, 0.0, 0.0, oMode);
            oDstMat = oTmpMat; // Prepare for next filter (return).

            if (oDstMat.data == oSrcMat.data)
            {
                LOG_ERROR("Resize() did not clone().");
            }
        }

        RETURN_IF_FAILED(WriteMat(m_pOutput, oDstMat, oTimestamp, oDstFormatName));
        RETURN_NOERROR;
    }

    /**
     * Compute best fitting image size after resize() operation
     *
     * @param[in] img_w Original image width in pixel [px].
     * @param[in] img_h Original image height in pixel [px].
     *
     * @return Returns best fitting image size.
     */
    cv::Size ComputeResizeSize( int32_t img_w, int32_t img_h ) const
    {
        int32_t dst_w = 0;
        int32_t dst_h = 0;
        int32_t const resize_w = std::max(0, static_cast<int32_t>(m_iResizeWidth));
        int32_t const resize_h = std::max(0, static_cast<int32_t>(m_iResizeHeight));

        if ((resize_w < 1) && (resize_h < 1))
        {
            // Resizing is disabled. 
            // Return original (here already cropped) image size.
            return cv::Size(img_w, img_h);
        }

        if (m_bResizeKeepAspectRatio)
        {
            if ((resize_w > 0) && (resize_h > 0))
            {
                // We best fit the image into the window (resize_w,resize_h).
                float const image_ratio = static_cast<float>(img_w) / static_cast<float>(img_h);
                float const abbox_ratio = static_cast<float>(resize_w) / static_cast<float>(resize_h);
                if (image_ratio >= abbox_ratio)
                {
                    // We give the image width all the available space.
                    //  w1              w2
                    // ---- = aspect = ---- ===>  y = w2 / aspect
                    //  h1              y
                    dst_w = resize_w;
                    dst_h = static_cast<int32_t>(static_cast<float>(resize_w) / image_ratio);
                }
                else
                {
                    // We give the image height all the available space.
                    //  w1              x
                    // ---- = aspect = ---- ===>  x = h2 * aspect 
                    //  h1              h2
                    dst_h = resize_h; 
                    dst_w = static_cast<int32_t>(image_ratio * resize_h);
                }
            }
            else
            {
                float const image_ratio = static_cast<float>(img_w) / static_cast<float>(img_h);
                if (resize_w > 0)
                {
                    // We give the image width all the available space.
                    //  w1              w2
                    // ---- = aspect = ---- ===>  y = w2 / aspect
                    //  h1              y
                    dst_w = resize_w;
                    dst_h = static_cast<int32_t>(static_cast<float>(resize_w) / image_ratio);
                }
                else
                {
                    // We give the image height all the available space.
                    //  w1              x
                    // ---- = aspect = ---- ===>  x = h2 * aspect 
                    //  h1              h2
                    dst_h = resize_h;
                    dst_w = static_cast<int32_t>(image_ratio * resize_h);
                }
            }
        }
        else
        {
            // We resize/stretch the image to cover entire user-given region size.
            if ((resize_w > 0) && (resize_h > 0))
            {
                dst_w = resize_w;
                dst_h = resize_h;
            }
            else
            {
                // We resize/stretch the image width and keep the original image height.
                if (resize_w > 0)
                {
                    dst_w = resize_w;
                    dst_h = img_h;
                }
                // We resize/stretch the image height and keep the original image width.
                else
                {
                    dst_w = img_w;
                    dst_h = resize_h;
                }
            }
        }

        return cv::Size(dst_w, dst_h);
    }
};
