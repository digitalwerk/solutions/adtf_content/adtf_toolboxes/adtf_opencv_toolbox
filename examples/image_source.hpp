/**
 *
 * @file
 * Copyright 2024 Digitalwerk GmbH.
 *
 *     This Source Code Form is subject to the terms of the Mozilla
 *     Public License, v. 2.0. If a copy of the MPL was not distributed
 *     with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * If it is not possible or desirable to put the notice in a particular file, then
 * You may include the notice in a location (such as a LICENSE file in a
 * relevant directory) where a recipient would be likely to look for such a notice.
 *
 * You may add additional accurate notices of copyright ownership.
 */

#pragma once

#include <adtffiltersdk/adtf_filtersdk.h>
#include <adtfsystemsdk/adtf_systemsdk.h>

#include <opencv_base_filter/opencv_samplebuffer.h>
#include <opencv_base_filter/opencv_base_filter.h>

#include <opencv2/opencv.hpp>

using namespace adtf::util;
using namespace adtf::ucom;
using namespace adtf::base;
using namespace adtf::streaming;
using namespace adtf::filter;
using namespace adtf::system;
using namespace adtf::opencvtb;

class cOpenCVImagesSource : public cFilter
{
public:
    ADTF_CLASS_ID_NAME(cOpenCVImagesSource,
        "opencv_image_reader.filter.dw.cid",
        "OpenCV Image Reader");

    ADTF_CLASS_DEPENDENCIES(REQUIRE_INTERFACE(adtf::services::IReferenceClock),
        REQUIRE_INTERFACE(adtf::services::IKernel));

public:
    property_variable<cFilepathList> m_strImageFolders;
    property_variable<int32_t> m_nWidth = 0;
    property_variable<int32_t> m_nHeight = 0;
    property_variable<bool> m_bGray = false;

    cPinWriter* m_pOutput;
    tStreamImageFormat m_sCurrentFormat;

    tUInt32 m_nIndex = 0;

    struct cListItem
    {
        tStreamImageFormat oHeader;
        object_ptr<ISample> pSample;
    };
    std::vector<cListItem> m_lstImages;

public:

    cOpenCVImagesSource()
    {
        SetHelpLink("$(ADTF_OPENCV_TOOLBOX_DIR)/doc/html/opencv_components_plugin_opencv_image_reader.html");
        SetDescription(
            "Use this Streaming Source to read images from files\n"
            "The image output can be connected to OpenCV filters.");

        m_strImageFolders.SetDescription("Directory with images.");
        RegisterPropertyVariable("image_folders", m_strImageFolders);
       
        SetDescription("image", "Captured images from files.");
        m_pOutput = CreateOutputPin("image");

        CreateRunner("capture", [this](tNanoSeconds oTimestamp)
        {
            return ProduceImage(oTimestamp);
        });

        SetDescription("capture", "Call capture on the opencv VideoCapture object and transmit the image sample");

        m_nHeight.SetDescription("Force height of images in [px]\n"
                                 "Default: 0 - Disabled resize");
        m_nWidth.SetDescription("Force width of images in [px]\n"
                                 "Default: 0 - Disabled resize");
        m_bGray.SetDescription("Force a grayscale 8-bit image");
        RegisterPropertyVariable("height", m_nHeight);
        RegisterPropertyVariable("width", m_nWidth);
        RegisterPropertyVariable("gray", m_bGray);
    }

    ~cOpenCVImagesSource()
    {
    }

    tResult Init(tInitStage eStage) override
    {
        RETURN_IF_FAILED(cFilter::Init(eStage));
        if (eStage == tInitStage::StageGraphReady)
        {
            for (auto const & strPath : *m_strImageFolders)
            {
                cStringList lstTempFiles;
                RETURN_IF_FAILED_DESC(cFileSystem::EnumDirectory(strPath, lstTempFiles), "Parsing folder %s failed", strPath.GetPtr());

                for (auto const & strFilename : lstTempFiles)
                {
                    cString strImagePath = strPath + "/" + strFilename;

                    cv::Mat oMat;
                    if (m_bGray)
                    {
                        oMat = cv::imread(strImagePath.GetPtr(), cv::IMREAD_GRAYSCALE);
                        if (oMat.channels() != 1)
                        {
                            LOG_ERROR("Did not convert to GREYSCALE_8");
                            continue;
                        }
                    }
                    else
                    {
                        oMat = cv::imread(strImagePath.GetPtr(), cv::IMREAD_COLOR);
                    }

                    if (oMat.empty())
                    {
                        LOG_ERROR("Captured image '%s' is empty", strImagePath.GetPtr());
                        continue;
                    }

                    if (!oMat.isContinuous())
                    {
                        LOG_ERROR("Captured image '%s' is not continuous", strImagePath.GetPtr());
                        //THROW_ERROR_DESC(ERR_FAILED, "Captured image '%s' is not continuous", strImagePath.GetPtr());
                        continue;
                    }

                    if (m_nWidth > 0 && m_nHeight > 0)
                    {
                        cv::Mat oTmpMat;
                        cv::resize(oMat, oTmpMat, cv::Size(m_nWidth, m_nHeight));
                        oMat = oTmpMat;
                    }

                    cListItem item;

                    if (IS_FAILED(createOpenCVSample(item.pSample, oMat, tNanoSeconds(0))))
                    {
                        continue;
                    }

                    item.oHeader = createCompatibleImageStreamFormat(oMat);
                    if (item.oHeader.m_strFormatName == "")
                    {
                        LOG_ERROR("Unsupported empty stream-format-name for cv::Mat type %" PRIu32, static_cast<uint32_t>(oMat.type()));
                        continue;
                    }

                    // Because of differing image sizes we store a tStreamImageFormat per image.
                    m_lstImages.emplace_back( std::move(item) );
                }
            }
        }

        RETURN_NOERROR;
    }

    cListItem GetNextImage()
    {
        if (m_lstImages.size() == 0)
        {
            return {};
        }

        if (m_nIndex >= m_lstImages.size())
        {
            m_nIndex = 0;
        }

        return m_lstImages.at(m_nIndex++);
    }

    tResult ProduceImage(adtf::base::tNanoSeconds oTimestamp)
    {
        cListItem item = GetNextImage();
        if (item.pSample)
        {
            set_sample_time(*item.pSample, oTimestamp);

            if (!isSameFormat(m_sCurrentFormat, item.oHeader))
            {
                object_ptr<IStreamType> pImageStreamType = make_object_ptr<cStreamType>(stream_meta_type_image());
                RETURN_IF_FAILED(set_stream_type_image_format(*pImageStreamType.Get(), item.oHeader));
                RETURN_IF_FAILED(m_pOutput->ChangeType(pImageStreamType));
                m_sCurrentFormat = item.oHeader;
            }

            m_pOutput->Write(item.pSample);
            m_pOutput->ManualTrigger();
        }
        else
        {
            THROW_ERROR_DESC(ERR_FAILED, "No image produced");
        }
        RETURN_NOERROR;
    }

};
