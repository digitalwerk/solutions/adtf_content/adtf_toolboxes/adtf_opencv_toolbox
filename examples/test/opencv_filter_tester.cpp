/**
 *
 * @file
 * Copyright 2024 Digitalwerk GmbH.
 *
 *     This Source Code Form is subject to the terms of the Mozilla
 *     Public License, v. 2.0. If a copy of the MPL was not distributed
 *     with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * If it is not possible or desirable to put the notice in a particular file, then
 * You may include the notice in a location (such as a LICENSE file in a
 * relevant directory) where a recipient would be likely to look for such a notice.
 *
 * You may add additional accurate notices of copyright ownership.
 */

#include <catch2/catch.hpp>
#include <adtf_ucom3.h>
#include <adtftesting/adtf_testing.h>
#include <adtfsystemsdk/testing/test_system.h>
#include <adtffiltersdk/adtf_filtersdk.h>
#include <opencv_base_filter/opencv_samplebuffer.h>
#include <opencv_base_filter/opencv_base_filter.h>
#include <opencv2/core.hpp>

using namespace adtf::util;
using namespace adtf::ucom;
using namespace adtf::base;
using namespace adtf::streaming;
using namespace adtf::filter;
using namespace adtf::services;
using namespace adtf::opencvtb;
using namespace cv;

struct cMyTestSystem: adtf::system::testing::cTestSystem
{
    cMyTestSystem()
    {
        LoadPlugin("opencv_components.adtfplugin");
    }
};

object_ptr<ISample> CreateImage(tInt32 nWidth, tInt32 nHeight)
{
    object_ptr<ISample> pSample;
    if (IS_OK(alloc_sample(pSample, 0)))
    {
        object_ptr_locked<ISampleBuffer> pBuffer;
        if (IS_OK(pSample->WriteLock(pBuffer, nWidth * nHeight * 3)))
        {
            tUInt8* pData = reinterpret_cast<tUInt8*>( pBuffer->GetPtr() );
            
            for (int i = 0; i < nHeight; i++)
            {
                for (int j = 0; j < nWidth; j++)
                {
                    pData[(i * nHeight + j) * 3] = 1;
                    pData[(i * nHeight + j) * 3 + 1] = 2;
                    pData[(i * nHeight + j) * 3 + 2] = 3;
                }
            }
        }
    }
    return pSample;
}

object_ptr<ISample> CreateMat24(tInt32 nWidth, tInt32 nHeight, std::chrono::nanoseconds nTimestamp)
{
    auto oMat = Mat(nHeight, nWidth, CV_8UC3, Scalar(1, 2, 3));
    object_ptr<ISample> pSample;
    if (IS_FAILED(createOpenCVSample(pSample,oMat,nTimestamp)))
    {
        THROW_ERROR_DESC(ERR_FAILED,"Failed to create buffered OpenCV Sample.");
    }
    return pSample;
}

object_ptr<ISample> CreateMat8(tInt32 nWidth, tInt32 nHeight, std::chrono::nanoseconds nTimestamp)
{
    auto oMat = Mat(nHeight, nWidth, CV_8UC1, Scalar(5, 4, 3));
    object_ptr<ISample> pSample;
    if (IS_FAILED(createOpenCVSample(pSample, oMat, nTimestamp)))
    {
        THROW_ERROR_DESC(ERR_FAILED, "Failed to create buffered OpenCV Sample.");
    }
    return pSample;
}

TEST_CASE_METHOD(cMyTestSystem, "cBayerPattern")
{
    adtf::ucom::object_ptr<adtf::streaming::IFilter> pFilter;
    REQUIRE_OK(_runtime->CreateInstance("opencv_bayer_pattern.filter.dw.cid", pFilter));

    IConfiguration* pConfiguration = ucom_cast<IConfiguration*>(pFilter.Get());
    adtf::ucom::object_ptr<IProperties> pProperties;    
    REQUIRE_OK(pConfiguration->GetProperties(pProperties));

    auto w = uint32_t(GENERATE(6, 4, 12));
    auto h = uint32_t(GENERATE(4, 6, 8));
    CAPTURE(w);
    CAPTURE(h);
    tStreamImageFormat oFormat;
    oFormat.m_strFormatName = ADTF_IMAGE_FORMAT(GREYSCALE_8);
    oFormat.m_ui32Width = w;
    oFormat.m_ui32Height = h;
    oFormat.m_szMaxByteSize = size_t(w) * h;

    object_ptr<IStreamType> pStreamType = make_object_ptr<cStreamType>(stream_meta_type_image());
    REQUIRE(IS_OK(set_stream_type_image_format(*pStreamType, oFormat)));

    REQUIRE_OK(get_stream_type_image_format(oFormat, *pStreamType));
    REQUIRE(oFormat.m_strFormatName == ADTF_IMAGE_FORMAT(GREYSCALE_8));
    REQUIRE(oFormat.m_ui32Width == w);
    REQUIRE(oFormat.m_ui32Height == h);
    REQUIRE(oFormat.m_szMaxByteSize == size_t(w) * h);

    REQUIRE_OK(pFilter->SetState(adtf::streaming::IFilter::tFilterState::State_Initialized));

    adtf::filter::testing::cTestWriter oInputWriter(pFilter, "mat_in", pStreamType);
    adtf::filter::testing::cOutputRecorder oOutputReader(pFilter, "mat_out");

    REQUIRE_OK(pFilter->SetState(adtf::streaming::IFilter::tFilterState::State_Running));

    auto pInputSample = CreateMat8(w, h, std::chrono::nanoseconds(1));
    oInputWriter.WriteSample(pInputSample);
    oInputWriter.ManualTrigger();

    auto oOutputSamples = oOutputReader.GetCurrentOutput().GetSamples();

    REQUIRE(oOutputSamples.size() == 1);

    object_ptr<const ISample> pImageSample = oOutputSamples.back();
    REQUIRE(pImageSample);

    object_ptr_shared_locked<const ISampleBuffer> pBuffer;
    REQUIRE(IS_OK(pImageSample->Lock(pBuffer)));

    REQUIRE(pBuffer->GetSize() == oFormat.m_szMaxByteSize * 3u);

    const tUInt8* pData = reinterpret_cast<const tUInt8*>(pBuffer->GetPtr());
    REQUIRE(pData != nullptr);

    for (size_t i = 0; i < size_t(w) * h; ++i)
    {
        REQUIRE(pData[i] == 5);
    }
}

TEST_CASE_METHOD(cMyTestSystem, "cResizeFilter")
{
    uint32_t w1;
    uint32_t h1;
    uint32_t w2;
    uint32_t h2;

    std::tie(w1,h1, w2,h2) = GENERATE(
        std::make_tuple(1,1, 13,16),
        std::make_tuple(2,3, 14,15),
        std::make_tuple(3,4, 15,14),
        std::make_tuple(5,2, 13,8));

    CAPTURE(w1);
    CAPTURE(h1);
    CAPTURE(w2);
    CAPTURE(h2);

    adtf::ucom::object_ptr<adtf::streaming::IFilter> pFilter;
    REQUIRE_OK(_runtime->CreateInstance("opencv_image_resizer.filter.dw.cid", pFilter));
    REQUIRE(pFilter->GetState() == adtf::streaming::IFilter::tFilterState::State_Shutdown);

    IConfiguration* pConfiguration = ucom_cast<IConfiguration*>(pFilter.Get());
    adtf::ucom::object_ptr<IProperties> pProperties;
    REQUIRE_OK(pConfiguration->GetProperties(pProperties));

    REQUIRE_OK(adtf::base::ant::set_property(*pProperties, "width", w2));
    REQUIRE_OK(adtf::base::ant::set_property(*pProperties, "height", h2));

    tStreamImageFormat oFormat;
    oFormat.m_strFormatName = ADTF_IMAGE_FORMAT(RGB_24);
    oFormat.m_ui32Width = w1;
    oFormat.m_ui32Height = h1;
    oFormat.m_szMaxByteSize = size_t(w1) * h1 * 3u;

    object_ptr<IStreamType> pStreamType = make_object_ptr<cStreamType>(stream_meta_type_image());
    REQUIRE(IS_OK(set_stream_type_image_format(*pStreamType, oFormat)));

    REQUIRE_OK(pFilter->SetState(adtf::streaming::IFilter::tFilterState::State_Initialized));

    adtf::filter::testing::cTestWriter oInputWriter(pFilter, "mat_in", pStreamType);
    adtf::filter::testing::cOutputRecorder oOutputReader(pFilter, "mat_out");

    REQUIRE_OK(pFilter->SetState(adtf::streaming::IFilter::tFilterState::State_Running));

    auto pInputSample = CreateMat24(w1, h1, std::chrono::nanoseconds(1));
    oInputWriter.WriteSample(pInputSample);
    oInputWriter.ManualTrigger();

    auto oOutputSamples = oOutputReader.GetCurrentOutput().GetSamples();

    REQUIRE(oOutputSamples.size() == 1);

    object_ptr<const ISample> pImageSample = oOutputSamples.back();
    REQUIRE(pImageSample);

    object_ptr_shared_locked<const ISampleBuffer> pBuffer;
    REQUIRE(IS_OK(pImageSample->Lock(pBuffer)));

    REQUIRE(pBuffer->GetSize() == size_t(w2)*h2*3u);

    const tUInt8* pData = reinterpret_cast<const tUInt8*>(pBuffer->GetPtr());
    REQUIRE(pData[0] == 1);
    REQUIRE(pData[1] == 2);
    REQUIRE(pData[2] == 3);
}

TEST_CASE_METHOD(cMyTestSystem, "cDNNOpenCVFilter", "[!mayfail]")
{
    adtf::ucom::object_ptr<adtf::streaming::IFilter> pFilter;
    REQUIRE_OK(_runtime->CreateInstance("opencv_dnn.filter.dw.cid", pFilter));

    auto w = uint32_t(7);
    auto h = uint32_t(4);

    tStreamImageFormat oFormat;
    oFormat.m_strFormatName = ADTF_IMAGE_FORMAT(BGR_24);
    oFormat.m_ui32Width = w;
    oFormat.m_ui32Height = h;
    oFormat.m_szMaxByteSize = size_t(w) * h * 3u;

    object_ptr<IStreamType> pStreamType = make_object_ptr<cStreamType>(stream_meta_type_image());
    REQUIRE(IS_OK(set_stream_type_image_format(*pStreamType, oFormat)));

    REQUIRE_OK(get_stream_type_image_format(oFormat, *pStreamType));
    CHECK(oFormat.m_strFormatName == ADTF_IMAGE_FORMAT(BGR_24));
    CHECK(oFormat.m_ui32Width == w);
    CHECK(oFormat.m_ui32Height == h);
    CHECK(oFormat.m_szMaxByteSize == size_t(w) * h * 3u);

    REQUIRE_OK(pFilter->SetState(adtf::streaming::IFilter::tFilterState::State_Initialized));

    adtf::filter::testing::cTestWriter oInputWriter(pFilter, "mat_in", pStreamType);
    adtf::filter::testing::cOutputRecorder oOutputReader(pFilter, "mat_out");

    REQUIRE_OK(pFilter->SetState(adtf::streaming::IFilter::tFilterState::State_Running));

    auto pInputSample = CreateMat24(w, h, std::chrono::nanoseconds(1));
    oInputWriter.WriteSample(pInputSample);
    oInputWriter.ManualTrigger();

    auto oOutputSamples = oOutputReader.GetCurrentOutput().GetSamples();

    REQUIRE(oOutputSamples.size() == 1);

    object_ptr<const ISample> pImageSample = oOutputSamples.back();
    REQUIRE(pImageSample);

    object_ptr_shared_locked<const ISampleBuffer> pBuffer;
    REQUIRE(IS_OK(pImageSample->Lock(pBuffer)));

    REQUIRE(pBuffer->GetSize() == oFormat.m_szMaxByteSize * 3u);
}


TEST_CASE_METHOD(cMyTestSystem, "cFaceDetectorFilter", "[!mayfail]")
{
    adtf::ucom::object_ptr<adtf::streaming::IFilter> pFilter;
    REQUIRE_OK(_runtime->CreateInstance("face_detector.filter.dw.cid", pFilter));

    auto w = uint32_t(6);
    auto h = uint32_t(5);

    tStreamImageFormat oFormat;
    oFormat.m_strFormatName = ADTF_IMAGE_FORMAT(GREYSCALE_8);
    oFormat.m_ui32Width = w;
    oFormat.m_ui32Height = h;
    oFormat.m_szMaxByteSize = size_t(w) * h;

    object_ptr<IStreamType> pStreamType = make_object_ptr<cStreamType>(stream_meta_type_image());
    REQUIRE_OK(set_stream_type_image_format(*pStreamType, oFormat));

    REQUIRE_OK(get_stream_type_image_format(oFormat, *pStreamType));
    REQUIRE(oFormat.m_strFormatName == ADTF_IMAGE_FORMAT(GREYSCALE_8));
    REQUIRE(oFormat.m_ui32Width == w);
    REQUIRE(oFormat.m_ui32Height == h);
    REQUIRE(oFormat.m_szMaxByteSize == size_t(w) * h);

    REQUIRE_OK(pFilter->SetState(adtf::streaming::IFilter::tFilterState::State_Initialized));

    adtf::filter::testing::cTestWriter oInputWriter(pFilter, "mat_in", pStreamType);
    adtf::filter::testing::cOutputRecorder oOutputReader(pFilter, "mat_out");

    REQUIRE_OK(pFilter->SetState(adtf::streaming::IFilter::tFilterState::State_Running));

    auto pInputSample = CreateMat8(w, h, std::chrono::nanoseconds(1));
    oInputWriter.WriteSample(pInputSample);
    oInputWriter.ManualTrigger();

    auto oOutputSamples = oOutputReader.GetCurrentOutput().GetSamples();
    REQUIRE(oOutputSamples.size() == 1);

    object_ptr<const ISample> pImageSample = oOutputSamples.back();
    REQUIRE(pImageSample);

    object_ptr_shared_locked<const ISampleBuffer> pBuffer;
    REQUIRE(IS_OK(pImageSample->Lock(pBuffer)));

    REQUIRE(pBuffer->GetSize() == oFormat.m_szMaxByteSize * 3u);

    const tUInt8* pData = reinterpret_cast<const tUInt8*>(pBuffer->GetPtr());
    REQUIRE(pData != nullptr);

    for (size_t i = 0; i < w * h; ++i)
    {
        REQUIRE(pData[i] == 5);
    }
}

TEST_CASE_METHOD(cMyTestSystem, "cTextFilter")
{
    uint32_t w = GENERATE(1, 2, 13, 16);
    uint32_t h = GENERATE(2, 3, 14, 15);
    CAPTURE(w);
    CAPTURE(h);

    adtf::ucom::object_ptr<adtf::streaming::IFilter> pFilter;
    REQUIRE_OK(_runtime->CreateInstance("opencv_text.filter.dw.cid", pFilter));
    REQUIRE(pFilter->GetState() == adtf::streaming::IFilter::tFilterState::State_Shutdown);

    IConfiguration* pConfiguration = ucom_cast<IConfiguration*>(pFilter.Get());
    adtf::ucom::object_ptr<IProperties> pProperties;
    REQUIRE_OK(pConfiguration->GetProperties(pProperties));
    REQUIRE_OK(adtf::base::ant::set_property(*pProperties, "width", w));
    REQUIRE_OK(adtf::base::ant::set_property(*pProperties, "height", h));

    tStreamImageFormat oFormat;
    oFormat.m_strFormatName = ADTF_IMAGE_FORMAT(RGB_24);
    oFormat.m_ui32Width = w;
    oFormat.m_ui32Height = h;
    oFormat.m_szMaxByteSize = size_t(w) * h * 3u;

    object_ptr<IStreamType> pStreamType = make_object_ptr<cStreamType>(stream_meta_type_image());
    REQUIRE(IS_OK(set_stream_type_image_format(*pStreamType, oFormat)));

    REQUIRE_OK(pFilter->SetState(adtf::streaming::IFilter::tFilterState::State_Initialized));

    adtf::filter::testing::cTestWriter oInputWriter(pFilter, "mat_in", pStreamType);
    adtf::filter::testing::cOutputRecorder oOutputReader(pFilter, "mat_out");

    REQUIRE_OK(pFilter->SetState(adtf::streaming::IFilter::tFilterState::State_Running));

    auto pInputSample = CreateMat24(w, h, std::chrono::nanoseconds(1));
    oInputWriter.WriteSample(pInputSample);
    oInputWriter.ManualTrigger();

    auto oOutputSamples = oOutputReader.GetCurrentOutput().GetSamples();

    REQUIRE(oOutputSamples.size() == 1);

    object_ptr<const ISample> pImageSample = oOutputSamples.back();
    REQUIRE(pImageSample);

    object_ptr_shared_locked<const ISampleBuffer> pBuffer;
    REQUIRE(IS_OK(pImageSample->Lock(pBuffer)));

    REQUIRE(pBuffer->GetSize() == size_t(w) * h * 3u);
}

TEST_CASE_METHOD(cMyTestSystem, "cTriggerCounter")
{
    adtf::ucom::object_ptr<adtf::streaming::IFilter> pFilter;
    REQUIRE_OK(_runtime->CreateInstance("trigger_counter.filter.dw.cid", pFilter));

    uint32_t triggerCounter = 0;

    IConfiguration* iConfiguration = ucom_cast<IConfiguration*>(pFilter.Get());
    adtf::ucom::object_ptr<IProperties> pProperties;
    REQUIRE_OK(iConfiguration->GetProperties(pProperties));
    REQUIRE_OK(adtf::base::ant::set_property(*pProperties, "counter", triggerCounter));
}

// [1.] Output format converter
enum class eOutputFormat { Original = 0, RGB_888, BGR_888, GREYSCALE_8 };

// [3.] Flip
enum class eFlip { Disabled = 0, Horizontal, Vertical, Both };

// [4.] Rotate
enum class eRotate { Disabled = 0, CW_90, CW_180, CW_270 };

struct cImageConverterFilterTester
{
    uint32_t w;
    uint32_t h;
    cv::Mat oMat;
    tStreamImageFormat oSrcFormat;
    adtf::ucom::object_ptr<adtf::streaming::IFilter> pFilter;
    IConfiguration* pConfiguration;
    adtf::ucom::object_ptr<IProperties> pProperties;
    object_ptr<IStreamType> pStreamType;
    std::unique_ptr<adtf::filter::testing::cTestWriter> pInputWriter;
    std::unique_ptr<adtf::filter::testing::cOutputRecorder> pOutputReader;
    object_ptr<ISample> pInputSample;
    object_ptr<const ISample> pOutputSample;
    object_ptr_shared_locked<const ISampleBuffer> pSampleBuffer;
    uint8_t const* pOutputData;

    //    2px
    // +---+---+
    // | R | G |
    // +---+---+
    // | B | Y | 3px
    // +---+---+
    // | W | M |
    // +---+---+
    static cv::Mat createMat2x3()
    {
        int w = 2;
        int h = 3;
        auto m = cv::Mat(h, w, CV_8UC3, Scalar(0, 0, 0));
        auto const stride = static_cast<size_t>(m.step);
        uint8_t* pDst = m.data;
        uint8_t* p = pDst;

        *p++ = 255; *p++ = 0; *p++ = 0;   // Red
        *p++ = 0; *p++ = 255; *p++ = 0;   // Green
        pDst += stride;
        p = pDst;
        *p++ = 0; *p++ = 0; *p++ = 255;   // Blue
        *p++ = 255; *p++ = 255; *p++ = 0; // Yellow
        pDst += stride;
        p = pDst;
        *p++ = 255; *p++ = 255; *p++ = 255; // White
        *p++ = 255; *p++ = 0; *p++ = 255; // Magenta

        return m;
    }

    tResult init( cv::Mat const & oSrcMat,
                  std::vector<std::pair<std::string, int32_t>> const & options)
    {
        w = static_cast<uint32_t>(oSrcMat.cols);
        h = static_cast<uint32_t>(oSrcMat.rows);
        oMat = oSrcMat;
        oSrcFormat.m_strFormatName = "";
        oSrcFormat.m_ui32Width = w;
        oSrcFormat.m_ui32Height = h;
        oSrcFormat.m_szMaxByteSize = 0;

        REQUIRE(oMat.depth() == CV_8U);

        if (oMat.channels() == 1)
        {
            oSrcFormat.m_strFormatName = ADTF_IMAGE_FORMAT(GREYSCALE_8);
        }
        else if (oMat.channels() == 3)
        {
            oSrcFormat.m_strFormatName = ADTF_IMAGE_FORMAT(RGB_24);
        }
        else if (oMat.channels() == 4)
        {
            oSrcFormat.m_strFormatName = ADTF_IMAGE_FORMAT(RGBA_32);
        }
        else
        {
            THROW_ERROR_DESC(ERR_FAILED, "Unsupported channel count %" PRIi32, oMat.channels());
        }
        oSrcFormat.m_szMaxByteSize = static_cast<size_t>(oMat.step) * h;

        REQUIRE_OK(_runtime->CreateInstance("opencv_image_converter.filter.dw.cid", pFilter));
        REQUIRE(pFilter->GetState() == adtf::streaming::IFilter::tFilterState::State_Shutdown);

        pConfiguration = ucom_cast<IConfiguration*>(pFilter.Get());
        REQUIRE_OK(pConfiguration->GetProperties(pProperties));

        for (auto const & option : options)
        {
            // TODO: set_property() seems to return OK even when property does not exist?
            REQUIRE_OK(adtf::base::set_property_by_path(*pProperties, option.first.c_str(), option.second));
        }

        pStreamType = make_object_ptr<cStreamType>(stream_meta_type_image());
        REQUIRE(IS_OK(set_stream_type_image_format(*pStreamType, oSrcFormat)));

        REQUIRE_OK(pFilter->SetState(adtf::streaming::IFilter::tFilterState::State_Initialized));

        pInputWriter = std::make_unique<adtf::filter::testing::cTestWriter>(pFilter, "mat_in", pStreamType);
        pOutputReader = std::make_unique<adtf::filter::testing::cOutputRecorder>(pFilter, "mat_out");

        REQUIRE_OK(pFilter->SetState(adtf::streaming::IFilter::tFilterState::State_Running));

    
        if (IS_FAILED(createOpenCVSample(pInputSample, oMat, std::chrono::nanoseconds(1))))
        {
            THROW_ERROR_DESC(ERR_FAILED, "Failed to create buffered OpenCV Sample.");
        }

        pInputWriter->WriteSample(pInputSample);
        pInputWriter->ManualTrigger();

        auto oOutputSamples = pOutputReader->GetCurrentOutput().GetSamples();

        REQUIRE(oOutputSamples.size() == 1);

        pOutputSample = oOutputSamples.back();
        REQUIRE(pOutputSample);

        REQUIRE(IS_OK(pOutputSample->Lock(pSampleBuffer)));

        pOutputData = reinterpret_cast<uint8_t const*>(pSampleBuffer->GetPtr());
        REQUIRE(pOutputData);

        RETURN_NOERROR;
    }
};

// ===============================================================================
//              Validate RGB_24 Mat 2x3
// ===============================================================================

// +---+---+
// | R | G |
// +---+---+
// | B | Y |
// +---+---+
// | W | M |
// +---+---+

TEST_CASE("OpenCV_Validate_Mat2x3","")
{
    cv::Mat m = cImageConverterFilterTester::createMat2x3();
    uint8_t const* p = m.data;

    uint32_t k = 0;
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 0);   REQUIRE(p[k + 2] == 0);   k += 3u; // Red
    REQUIRE(p[k + 0] == 0);   REQUIRE(p[k + 1] == 255); REQUIRE(p[k + 2] == 0);   k += 3u; // Green
    REQUIRE(p[k + 0] == 0);   REQUIRE(p[k + 1] == 0);   REQUIRE(p[k + 2] == 255); k += 3u; // Blue
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 255); REQUIRE(p[k + 2] == 0);   k += 3u; // Yellow
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 255); REQUIRE(p[k + 2] == 255); k += 3u; // White
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 0);   REQUIRE(p[k + 2] == 255); k += 3u; // Magenta
}

// ===============================================================================
// Relay case: Output-sample-buffer must be input-sample-buffer.
// ===============================================================================

//    RGB                                   RGB
// +---+---+                             +---+---+
// | R | G |                             | R | G |
// +---+---+        Relay = No op        +---+---+
// | B | Y |  ------------------------>  | B | Y |
// +---+---+                             +---+---+
// | W | M |                             | W | M |
// +---+---+                             +---+---+

TEST_CASE_METHOD(cMyTestSystem, "cImageConverter_Relay")
{
    cv::Mat m1 = cImageConverterFilterTester::createMat2x3();

    cImageConverterFilterTester sys;
    sys.init(m1, {});
    uint8_t const* p = sys.pOutputData;

    CHECK( m1.data == p );

    uint32_t k = 0;
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 0);   REQUIRE(p[k + 2] == 0);   k += 3u; // Red
    REQUIRE(p[k + 0] == 0);   REQUIRE(p[k + 1] == 255); REQUIRE(p[k + 2] == 0);   k += 3u; // Green
    REQUIRE(p[k + 0] == 0);   REQUIRE(p[k + 1] == 0);   REQUIRE(p[k + 2] == 255); k += 3u; // Blue
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 255); REQUIRE(p[k + 2] == 0);   k += 3u; // Yellow
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 255); REQUIRE(p[k + 2] == 255); k += 3u; // White
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 0);   REQUIRE(p[k + 2] == 255); k += 3u; // Magenta
}

// ===============================================================================
//              OutputFormatConverter
// ===============================================================================

//    RGB                                   BGR
// +---+---+                             +---+---+
// | R | G |                             | B | G |
// +---+---+   eOutputFormat::BGR_888    +---+---+
// | B | Y |  ------------------------>  | R | C |
// +---+---+                             +---+---+
// | W | M |                             | W | M |
// +---+---+                             +---+---+

TEST_CASE_METHOD(cMyTestSystem, "cImageConverter_OutputFormat_RGB_2_BGR")
{
    cImageConverterFilterTester sys;
    sys.init(cImageConverterFilterTester::createMat2x3(), { 
        {"output_format",           int32_t(eOutputFormat::BGR_888)} });
    uint8_t const* p = sys.pOutputData;

    uint32_t k = 0;
    REQUIRE(p[k + 0] == 0);   REQUIRE(p[k + 1] == 0);   REQUIRE(p[k + 2] == 255); k += 3u; // Blue
    REQUIRE(p[k + 0] == 0);   REQUIRE(p[k + 1] == 255); REQUIRE(p[k + 2] == 0);   k += 3u; // Green
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 0);   REQUIRE(p[k + 2] == 0);   k += 3u; // Red
    REQUIRE(p[k + 0] == 0);   REQUIRE(p[k + 1] == 255); REQUIRE(p[k + 2] == 255); k += 3u; // Cyan
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 255); REQUIRE(p[k + 2] == 255); k += 3u; // White
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 0);   REQUIRE(p[k + 2] == 255); k += 3u; // Magenta
}

// ===============================================================================
//              CropLeft
// ===============================================================================

// +--- data                            +--- data
// |                                    |
// v                                    v
// +---+---+ stride = 6             +---+---+    stride is still 6
// | R | G |                        |   | G |
// +---+---+    crop-left = 1       +---+---+
// | B | Y | ------------------->   |   | Y |
// +---+---+                        +---+---+
// | W | M |                        |   | M |
// +---+---+                        +---+---+

TEST_CASE("OpenCV_CropLeft_1px","")
{
    auto m1 = cImageConverterFilterTester::createMat2x3();
    auto m2 = m1(cv::Rect(1,0,1,3));

    CHECK(m1.datastart == m2.datastart);
    CHECK(m1.data != m2.data);

    // Validate pixels:
    auto p = m2.data;
    uint32_t k = 0;

    // Green
    CHECK(uint32_t(p[k + 0]) == 0u);
    CHECK(uint32_t(p[k + 1]) == 255u);
    CHECK(uint32_t(p[k + 2]) == 0u);
    k += 6u;

    // Yellow
    CHECK(uint32_t(p[k + 0]) == 255u);
    CHECK(uint32_t(p[k + 1]) == 255u);
    CHECK(uint32_t(p[k + 2]) == 0u);
    k += 6u;

    // Magenta
    CHECK(uint32_t(p[k + 0]) == 255u);
    CHECK(uint32_t(p[k + 1]) == 0u);
    CHECK(uint32_t(p[k + 2]) == 255u);
    k += 6u;
}

// +---+---+                        +---+
// | R | G |                        | G |
// +---+---+    crop/left = 1       +---+
// | B | Y | ------------------->   | Y |
// +---+---+                        +---+
// | W | M |                        | M |
// +---+---+                        +---+

TEST_CASE_METHOD(cMyTestSystem, "cImageConverter_CropLeft_1px")
{
    cImageConverterFilterTester sys;
    sys.init(cImageConverterFilterTester::createMat2x3(), { 
        {"crop/left",               1 } });
    uint8_t const* p = sys.pOutputData;

    uint32_t k = 0;

    // Green
    CHECK(uint32_t(p[k + 0]) == 0u);
    CHECK(uint32_t(p[k + 1]) == 255u);
    CHECK(uint32_t(p[k + 2]) == 0u);
    k += 3u;

    // Yellow
    CHECK(uint32_t(p[k + 0]) == 255u);
    CHECK(uint32_t(p[k + 1]) == 255u);
    CHECK(uint32_t(p[k + 2]) == 0u);
    k += 3u;
    
    // Magenta
    CHECK(uint32_t(p[k + 0]) == 255u);
    CHECK(uint32_t(p[k + 1]) == 0u);
    CHECK(uint32_t(p[k + 2]) == 255u);
    k += 3u;
}

// ===============================================================================
//              CropRight
// ===============================================================================

// +---+---+                        +---+
// | R | G |                        | R |
// +---+---+    crop/right = 1      +---+
// | B | Y | ------------------->   | B |
// +---+---+                        +---+
// | W | M |                        | W |
// +---+---+                        +---+

TEST_CASE_METHOD(cMyTestSystem, "cImageConverter_CropRight_1px")
{
    cImageConverterFilterTester sys;
    sys.init(cImageConverterFilterTester::createMat2x3(), { 
        {"crop/right", 1 } });
    uint8_t const* p = sys.pOutputData;

    uint32_t k = 0;

    // Red
    REQUIRE(p[k + 0] == 255); 
    REQUIRE(p[k + 1] == 0);   
    REQUIRE(p[k + 2] == 0);   
    k += 3u;
    
    // Blue
    REQUIRE(p[k + 0] == 0);   
    REQUIRE(p[k + 1] == 0);   
    REQUIRE(p[k + 2] == 255); 
    k += 3u;
    
    // White
    REQUIRE(p[k + 0] == 255); 
    REQUIRE(p[k + 1] == 255); 
    REQUIRE(p[k + 2] == 255); 
    k += 3u;
}

// ===============================================================================
//              eRotate::Disabled
// ===============================================================================

// +---+---+                        +---+---+
// | R | G |                        | R | G |
// +---+---+  eRotate::Disabled     +---+---+
// | B | Y | ------------------->   | B | Y |
// +---+---+   eFlip::Disabled      +---+---+
// | W | M |                        | W | M |
// +---+---+                        +---+---+

TEST_CASE_METHOD(cMyTestSystem, "cImageConverter_00_eRotateDisabled_AND_eFlipDisabled")
{
    cImageConverterFilterTester sys;
    sys.init( cImageConverterFilterTester::createMat2x3(), { 
        {"rotate", int32_t(eRotate::Disabled)},
        {"flip",   int32_t(eFlip::Disabled)  } });
    uint8_t const * p = sys.pOutputData;

    uint32_t k = 0;
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 0);   REQUIRE(p[k + 2] == 0);   k += 3u; // Red
    REQUIRE(p[k + 0] == 0);   REQUIRE(p[k + 1] == 255); REQUIRE(p[k + 2] == 0);   k += 3u; // Green
    REQUIRE(p[k + 0] == 0);   REQUIRE(p[k + 1] == 0);   REQUIRE(p[k + 2] == 255); k += 3u; // Blue
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 255); REQUIRE(p[k + 2] == 0);   k += 3u; // Yellow
}

// +---+---+                        +---+---+
// | R | G |                        | G | R |
// +---+---+  eRotate::Disabled     +---+---+
// | B | Y | ------------------->   | Y | B |
// +---+---+  eFlip::Horizontal     +---+---+
// | W | M |                        | M | W |
// +---+---+                        +---+---+

TEST_CASE_METHOD(cMyTestSystem, "cImageConverter_01_eRotateDisabled_AND_eFlipH")
{
    cImageConverterFilterTester sys;
    sys.init( cImageConverterFilterTester::createMat2x3(), { 
        {"rotate", int32_t(eRotate::Disabled)},
        {"flip",   int32_t(eFlip::Horizontal)} });
    uint8_t const* p = sys.pOutputData;

    uint32_t k = 0;
    REQUIRE(p[k + 0] == 0);   REQUIRE(p[k + 1] == 255); REQUIRE(p[k + 2] == 0);   k += 3u; // Green
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 0);   REQUIRE(p[k + 2] == 0);   k += 3u; // Red
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 255); REQUIRE(p[k + 2] == 0);   k += 3u; // Yellow
    REQUIRE(p[k + 0] == 0);   REQUIRE(p[k + 1] == 0);   REQUIRE(p[k + 2] == 255); k += 3u; // Blue
}

// +---+---+                        +---+---+
// | R | G |                        | W | M |
// +---+---+  eRotate::Disabled     +---+---+
// | B | Y | ------------------->   | B | Y |
// +---+---+   eFlip::Vertical      +---+---+
// | W | M |                        | R | G |
// +---+---+                        +---+---+

TEST_CASE_METHOD(cMyTestSystem, "cImageConverter_02_eRotateDisabled_AND_eFlipV")
{
    cImageConverterFilterTester sys;
    sys.init( cImageConverterFilterTester::createMat2x3(), { 
        {"rotate", int32_t(eRotate::Disabled)},
        {"flip",   int32_t(eFlip::Vertical)  } });
    uint8_t const* p = sys.pOutputData;

    uint32_t k = 0;
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 255); REQUIRE(p[k + 2] == 255); k += 3u; // White
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 0);   REQUIRE(p[k + 2] == 255); k += 3u; // Magenta
    REQUIRE(p[k + 0] == 0);   REQUIRE(p[k + 1] == 0);   REQUIRE(p[k + 2] == 255); k += 3u; // Blue
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 255); REQUIRE(p[k + 2] == 0);   k += 3u; // Yellow
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 0);   REQUIRE(p[k + 2] == 0);   k += 3u; // Red
    REQUIRE(p[k + 0] == 0);   REQUIRE(p[k + 1] == 255); REQUIRE(p[k + 2] == 0);   k += 3u; // Green
}

// +---+---+                        +---+---+
// | R | G |                        | M | W |
// +---+---+  eRotate::Disabled     +---+---+
// | B | Y | ------------------->   | Y | B |
// +---+---+    eFlip::Both         +---+---+
// | W | M |                        | G | R |
// +---+---+                        +---+---+

TEST_CASE_METHOD(cMyTestSystem, "cImageConverter_03_eRotateDisabled_AND_eFlipBoth")
{
    cImageConverterFilterTester sys;
    sys.init( cImageConverterFilterTester::createMat2x3(), { 
        {"rotate", int32_t(eRotate::Disabled)},
        {"flip",   int32_t(eFlip::Both)      } });
    uint8_t const* p = sys.pOutputData;

    uint32_t k = 0;
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 0);   REQUIRE(p[k + 2] == 255); k += 3u; // Magenta
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 255); REQUIRE(p[k + 2] == 255); k += 3u; // White
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 255); REQUIRE(p[k + 2] == 0);   k += 3u; // Yellow
    REQUIRE(p[k + 0] == 0);   REQUIRE(p[k + 1] == 0);   REQUIRE(p[k + 2] == 255); k += 3u; // Blue
    REQUIRE(p[k + 0] == 0);   REQUIRE(p[k + 1] == 255); REQUIRE(p[k + 2] == 0);   k += 3u; // Green
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 0);   REQUIRE(p[k + 2] == 0);   k += 3u; // Red
}

// ===============================================================================
//              eRotate::CW_90
// ===============================================================================

// +---+---+                        
// | R | G |                        +---+---+---+
// +---+---+    eRotate::CW_90      | W | B | R |
// | B | Y | ------------------->   +---+---+---+   
// +---+---+    eFlip::Disabled     | M | Y | G |
// | W | M |                        +---+---+---+
// +---+---+                        

TEST_CASE_METHOD(cMyTestSystem, "cImageConverter_10_eRotateCW90_AND_eFlipDisabled")
{
    cImageConverterFilterTester sys;
    sys.init( cImageConverterFilterTester::createMat2x3(), { 
        {"rotate", int32_t(eRotate::CW_90) },
        {"flip",   int32_t(eFlip::Disabled)} });
    uint8_t const* p = sys.pOutputData;

    uint32_t k = 0;
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 255); REQUIRE(p[k + 2] == 255); k += 3u; // White
    REQUIRE(p[k + 0] == 0);   REQUIRE(p[k + 1] == 0);   REQUIRE(p[k + 2] == 255); k += 3u; // Blue
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 0);   REQUIRE(p[k + 2] == 0);   k += 3u; // Red
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 0);   REQUIRE(p[k + 2] == 255); k += 3u; // Magenta
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 255); REQUIRE(p[k + 2] == 0);   k += 3u; // Yellow
    REQUIRE(p[k + 0] == 0);   REQUIRE(p[k + 1] == 255); REQUIRE(p[k + 2] == 0);   k += 3u; // Green
}

// +---+---+
// | R | G |                        +---+---+---+
// +---+---+    eRotate::CW_90      | R | B | W |
// | B | Y | ------------------->   +---+---+---+   
// +---+---+   eFlip::Horizontal    | G | Y | M |
// | W | M |                        +---+---+---+
// +---+---+

TEST_CASE_METHOD(cMyTestSystem, "cImageConverter_11_eRotateCW90_AND_eFlipH")
{
    cImageConverterFilterTester sys;
    sys.init( cImageConverterFilterTester::createMat2x3(), { 
        {"rotate", int32_t(eRotate::CW_90)   },
        {"flip",   int32_t(eFlip::Horizontal)} });
    uint8_t const* p = sys.pOutputData;

    uint32_t k = 0;
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 0);   REQUIRE(p[k + 2] == 0);   k += 3u; // Red
    REQUIRE(p[k + 0] == 0);   REQUIRE(p[k + 1] == 0);   REQUIRE(p[k + 2] == 255); k += 3u; // Blue
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 255); REQUIRE(p[k + 2] == 255); k += 3u; // White
    REQUIRE(p[k + 0] == 0);   REQUIRE(p[k + 1] == 255); REQUIRE(p[k + 2] == 0);   k += 3u; // Green
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 255); REQUIRE(p[k + 2] == 0);   k += 3u; // Yellow
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 0);   REQUIRE(p[k + 2] == 255); k += 3u; // Magenta
}

// +---+---+                        
// | R | G |                        +---+---+---+
// +---+---+    eRotate::CW_90      | M | Y | G |
// | B | Y | ------------------->   +---+---+---+   
// +---+---+    eFlip::Vertical     | W | B | R |
// | W | M |                        +---+---+---+
// +---+---+                        

TEST_CASE_METHOD(cMyTestSystem, "cImageConverter_12_eRotateCW90_AND_eFlipV")
{
    cImageConverterFilterTester sys;
    sys.init( cImageConverterFilterTester::createMat2x3(), { 
        {"rotate", int32_t(eRotate::CW_90) },
        {"flip",   int32_t(eFlip::Vertical)} });
    uint8_t const* p = sys.pOutputData;

    uint32_t k = 0;
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 0);   REQUIRE(p[k + 2] == 255); k += 3u; // Magenta
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 255); REQUIRE(p[k + 2] == 0);   k += 3u; // Yellow
    REQUIRE(p[k + 0] == 0);   REQUIRE(p[k + 1] == 255); REQUIRE(p[k + 2] == 0);   k += 3u; // Green
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 255); REQUIRE(p[k + 2] == 255); k += 3u; // White
    REQUIRE(p[k + 0] == 0);   REQUIRE(p[k + 1] == 0);   REQUIRE(p[k + 2] == 255); k += 3u; // Blue
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 0);   REQUIRE(p[k + 2] == 0);   k += 3u; // Red
}

// +---+---+                        
// | R | G |                        +---+---+---+
// +---+---+    eRotate::CW_90      | G | Y | M |
// | B | Y | ------------------->   +---+---+---+
// +---+---+      eFlip::Both       | R | B | W |
// | W | M |                        +---+---+---+
// +---+---+                        

TEST_CASE_METHOD(cMyTestSystem, "cImageConverter_13_eRotateCW90_AND_eFlipBoth")
{
    cImageConverterFilterTester sys;
    sys.init( cImageConverterFilterTester::createMat2x3(), { 
        {"rotate", int32_t(eRotate::CW_90)},
        {"flip",   int32_t(eFlip::Both)   } });
    uint8_t const* p = sys.pOutputData;

    uint32_t k = 0;
    REQUIRE(p[k + 0] == 0);   REQUIRE(p[k + 1] == 255); REQUIRE(p[k + 2] == 0);   k += 3u; // Green
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 255); REQUIRE(p[k + 2] == 0);   k += 3u; // Yellow
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 0);   REQUIRE(p[k + 2] == 255); k += 3u; // Magenta
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 0);   REQUIRE(p[k + 2] == 0);   k += 3u; // Red
    REQUIRE(p[k + 0] == 0);   REQUIRE(p[k + 1] == 0);   REQUIRE(p[k + 2] == 255); k += 3u; // Blue
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 255); REQUIRE(p[k + 2] == 255); k += 3u; // White
}


// ===============================================================================
//              eRotate::CW_180
// ===============================================================================

// +---+---+                        +---+---+
// | R | G |                        | M | W |
// +---+---+   eRotate::CW_180      +---+---+
// | B | Y | ------------------->   | Y | B |
// +---+---+   eFlip::Disabled      +---+---+
// | W | M |                        | G | R |
// +---+---+                        +---+---+
TEST_CASE_METHOD(cMyTestSystem, "cImageConverter_20_eRotateCW180_AND_eFlipDisabled")
{
    cImageConverterFilterTester sys;
    sys.init( cImageConverterFilterTester::createMat2x3(), { 
        {"rotate", int32_t(eRotate::CW_180)},
        {"flip",   int32_t(eFlip::Disabled)} });
    uint8_t const* p = sys.pOutputData;

    uint32_t k = 0;
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 0);   REQUIRE(p[k + 2] == 255); k += 3u; // Magenta
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 255); REQUIRE(p[k + 2] == 255); k += 3u; // White
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 255); REQUIRE(p[k + 2] == 0);   k += 3u; // Yellow
    REQUIRE(p[k + 0] == 0);   REQUIRE(p[k + 1] == 0);   REQUIRE(p[k + 2] == 255); k += 3u; // Blue
    REQUIRE(p[k + 0] == 0);   REQUIRE(p[k + 1] == 255); REQUIRE(p[k + 2] == 0);   k += 3u; // Green
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 0);   REQUIRE(p[k + 2] == 0);   k += 3u; // Red
}

// +---+---+                        +---+---+
// | R | G |                        | W | M |
// +---+---+   eRotate::CW_180      +---+---+
// | B | Y | ------------------->   | B | Y |
// +---+---+   eFlip::Horizontal    +---+---+
// | W | M |                        | R | G |
// +---+---+                        +---+---+
TEST_CASE_METHOD(cMyTestSystem, "cImageConverter_21_eRotateCW180_AND_eFlipH")
{
    cImageConverterFilterTester sys;
    sys.init( cImageConverterFilterTester::createMat2x3(), { 
        {"rotate", int32_t(eRotate::CW_180)  },
        {"flip",   int32_t(eFlip::Horizontal)} });
    uint8_t const* p = sys.pOutputData;

    uint32_t k = 0;
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 255); REQUIRE(p[k + 2] == 255); k += 3u; // White
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 0);   REQUIRE(p[k + 2] == 255); k += 3u; // Magenta
    REQUIRE(p[k + 0] == 0);   REQUIRE(p[k + 1] == 0);   REQUIRE(p[k + 2] == 255); k += 3u; // Blue
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 255); REQUIRE(p[k + 2] == 0);   k += 3u; // Yellow
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 0);   REQUIRE(p[k + 2] == 0);   k += 3u; // Red
    REQUIRE(p[k + 0] == 0);   REQUIRE(p[k + 1] == 255); REQUIRE(p[k + 2] == 0);   k += 3u; // Green
}

// +---+---+                        +---+---+
// | R | G |                        | G | R |
// +---+---+   eRotate::CW_180      +---+---+
// | B | Y | ------------------->   | Y | B |
// +---+---+   eFlip::Vertical      +---+---+
// | W | M |                        | M | W |
// +---+---+                        +---+---+
TEST_CASE_METHOD(cMyTestSystem, "cImageConverter_22_eRotateCW180_AND_eFlipV")
{
    cImageConverterFilterTester sys;
    sys.init( cImageConverterFilterTester::createMat2x3(), { 
        {"rotate", int32_t(eRotate::CW_180)},
        {"flip",   int32_t(eFlip::Vertical)} });
    uint8_t const* p = sys.pOutputData;

    uint32_t k = 0;
    REQUIRE(p[k + 0] == 0);   REQUIRE(p[k + 1] == 255); REQUIRE(p[k + 2] == 0);   k += 3u; // Green
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 0);   REQUIRE(p[k + 2] == 0);   k += 3u; // Red
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 255); REQUIRE(p[k + 2] == 0);   k += 3u; // Yellow
    REQUIRE(p[k + 0] == 0);   REQUIRE(p[k + 1] == 0);   REQUIRE(p[k + 2] == 255); k += 3u; // Blue
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 0);   REQUIRE(p[k + 2] == 255); k += 3u; // Magenta
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 255); REQUIRE(p[k + 2] == 255); k += 3u; // White
}

// +---+---+                        +---+---+
// | R | G |                        | R | G |
// +---+---+   eRotate::CW_180      +---+---+
// | B | Y | ------------------->   | B | Y |
// +---+---+     eFlip::Both        +---+---+
// | W | M |                        | W | M |
// +---+---+                        +---+---+
TEST_CASE_METHOD(cMyTestSystem, "cImageConverter_23_eRotateCW180_AND_eFlipBoth")
{
    cImageConverterFilterTester sys;
    sys.init( cImageConverterFilterTester::createMat2x3(), { 
        {"rotate", int32_t(eRotate::CW_180)},
        {"flip",   int32_t(eFlip::Both)    } });
    uint8_t const* p = sys.pOutputData;

    uint32_t k = 0;
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 0);   REQUIRE(p[k + 2] == 0);   k += 3u; // Red
    REQUIRE(p[k + 0] == 0);   REQUIRE(p[k + 1] == 255); REQUIRE(p[k + 2] == 0);   k += 3u; // Green
    REQUIRE(p[k + 0] == 0);   REQUIRE(p[k + 1] == 0);   REQUIRE(p[k + 2] == 255); k += 3u; // Blue
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 255); REQUIRE(p[k + 2] == 0);   k += 3u; // Yellow
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 255); REQUIRE(p[k + 2] == 255); k += 3u; // White
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 0);   REQUIRE(p[k + 2] == 255); k += 3u; // Magenta
}

// ===============================================================================
//              eRotate::CW_270
// ===============================================================================

// +---+---+
// | R | G |                        +---+---+---+
// +---+---+    eRotate::CW_270     | G | Y | M |
// | B | Y | ------------------->   +---+---+---+
// +---+---+    eFlip::Disabled     | R | B | W |
// | W | M |                        +---+---+---+
// +---+---+
TEST_CASE_METHOD(cMyTestSystem, "cImageConverter_30_eRotateCW270_AND_eFlipDisabled")
{
    cImageConverterFilterTester sys;
    sys.init( cImageConverterFilterTester::createMat2x3(), { 
        {"rotate", int32_t(eRotate::CW_270)},
        {"flip",   int32_t(eFlip::Disabled)} });
    uint8_t const* p = sys.pOutputData;

    uint32_t k = 0;
    REQUIRE(p[k + 0] == 0);   REQUIRE(p[k + 1] == 255); REQUIRE(p[k + 2] == 0);   k += 3u; // Green
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 255); REQUIRE(p[k + 2] == 0);   k += 3u; // Yellow
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 0);   REQUIRE(p[k + 2] == 255); k += 3u; // Magenta
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 0);   REQUIRE(p[k + 2] == 0);   k += 3u; // Red
    REQUIRE(p[k + 0] == 0);   REQUIRE(p[k + 1] == 0);   REQUIRE(p[k + 2] == 255); k += 3u; // Blue
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 255); REQUIRE(p[k + 2] == 255); k += 3u; // White
}

// +---+---+
// | R | G |                        +---+---+---+
// +---+---+    eRotate::CW_270     | M | Y | G |
// | B | Y | ------------------->   +---+---+---+   
// +---+---+   eFlip::Horizontal    | W | B | R |
// | W | M |                        +---+---+---+
// +---+---+
TEST_CASE_METHOD(cMyTestSystem, "cImageConverter_31_eRotateCW270_AND_eFlipH")
{
    cImageConverterFilterTester sys;
    sys.init( cImageConverterFilterTester::createMat2x3(), { 
        {"rotate", int32_t(eRotate::CW_270)  },
        {"flip",   int32_t(eFlip::Horizontal)} });
    uint8_t const* p = sys.pOutputData;

    uint32_t k = 0;
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 0);   REQUIRE(p[k + 2] == 255); k += 3u; // Magenta
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 255); REQUIRE(p[k + 2] == 0);   k += 3u; // Yellow
    REQUIRE(p[k + 0] == 0);   REQUIRE(p[k + 1] == 255); REQUIRE(p[k + 2] == 0);   k += 3u; // Green
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 255); REQUIRE(p[k + 2] == 255); k += 3u; // White
    REQUIRE(p[k + 0] == 0);   REQUIRE(p[k + 1] == 0);   REQUIRE(p[k + 2] == 255); k += 3u; // Blue
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 0);   REQUIRE(p[k + 2] == 0);   k += 3u; // Red
}

// +---+---+
// | R | G |                        +---+---+---+
// +---+---+    eRotate::CW_270     | R | B | W |
// | B | Y | ------------------->   +---+---+---+   
// +---+---+    eFlip::Vertical     | G | Y | M |
// | W | M |                        +---+---+---+
// +---+---+
TEST_CASE_METHOD(cMyTestSystem, "cImageConverter_32_eRotateCW270_AND_eFlipV")
{
    cImageConverterFilterTester sys;
    sys.init( cImageConverterFilterTester::createMat2x3(), { 
        {"rotate", int32_t(eRotate::CW_270)},
        {"flip",   int32_t(eFlip::Vertical)} });
    uint8_t const* p = sys.pOutputData;

    uint32_t k = 0;
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 0);   REQUIRE(p[k + 2] == 0);   k += 3u; // Red
    REQUIRE(p[k + 0] == 0);   REQUIRE(p[k + 1] == 0);   REQUIRE(p[k + 2] == 255); k += 3u; // Blue
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 255); REQUIRE(p[k + 2] == 255); k += 3u; // White
    REQUIRE(p[k + 0] == 0);   REQUIRE(p[k + 1] == 255); REQUIRE(p[k + 2] == 0);   k += 3u; // Green
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 255); REQUIRE(p[k + 2] == 0);   k += 3u; // Yellow
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 0);   REQUIRE(p[k + 2] == 255); k += 3u; // Magenta
}

// +---+---+
// | R | G |                        +---+---+---+
// +---+---+    eRotate::CW_270     | W | B | R |
// | B | Y | ------------------->   +---+---+---+   
// +---+---+      eFlip::Both       | M | Y | G |
// | W | M |                        +---+---+---+
// +---+---+
TEST_CASE_METHOD(cMyTestSystem, "cImageConverter_33_eRotateCW270_AND_eFlipBoth")
{
    cImageConverterFilterTester sys;
    sys.init( cImageConverterFilterTester::createMat2x3(), { 
        {"rotate", int32_t(eRotate::CW_270)},
        {"flip",   int32_t(eFlip::Both)    } });
    uint8_t const* p = sys.pOutputData;

    uint32_t k = 0;
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 255); REQUIRE(p[k + 2] == 255); k += 3u; // White
    REQUIRE(p[k + 0] == 0);   REQUIRE(p[k + 1] == 0);   REQUIRE(p[k + 2] == 255); k += 3u; // Blue
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 0);   REQUIRE(p[k + 2] == 0);   k += 3u; // Red
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 0);   REQUIRE(p[k + 2] == 255); k += 3u; // Magenta
    REQUIRE(p[k + 0] == 255); REQUIRE(p[k + 1] == 255); REQUIRE(p[k + 2] == 0);   k += 3u; // Yellow
    REQUIRE(p[k + 0] == 0);   REQUIRE(p[k + 1] == 255); REQUIRE(p[k + 2] == 0);   k += 3u; // Green
}
