/**
 *
 * @file
 * Copyright 2024 Digitalwerk GmbH.
 *
 *     This Source Code Form is subject to the terms of the Mozilla
 *     Public License, v. 2.0. If a copy of the MPL was not distributed
 *     with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * If it is not possible or desirable to put the notice in a particular file, then
 * You may include the notice in a location (such as a LICENSE file in a
 * relevant directory) where a recipient would be likely to look for such a notice.
 *
 * You may add additional accurate notices of copyright ownership.
 */

#include <adtffiltersdk/adtf_filtersdk.h>
#include <adtfsystemsdk/adtf_systemsdk.h>

#include <opencv_base_filter/opencv_base_filter.h>

#include <opencv2/opencv.hpp>
#include <opencv2/dnn.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/objdetect.hpp>

#include <cmath>

using namespace adtf::util;
using namespace adtf::ucom;
using namespace adtf::base;
using namespace adtf::streaming;
using namespace adtf::filter;
using namespace adtf::system;

using namespace cv::dnn;
using namespace cv;
using namespace std;

using namespace adtf::opencvtb;

class cFaceDetectorFilter : public cOpenCVBaseFilter
{
public: 
    ADTF_CLASS_ID_NAME(cFaceDetectorFilter,
        "face_detector.filter.dw.cid",
        "OpenCV Face Detector Filter");

public:
    property_variable<cFilename> m_strFDModel;
    property_variable<cFilename> m_strFRModel;

    property_variable<tFloat32> m_fScoreThreshold = 0.9f;
    property_variable<tFloat32> m_fNMSThreshold = 0.3f;
    property_variable<tFloat32> m_fScale = 1.f;

    property_variable<tInt32> m_nTopK = 5000;
    property_variable<tInt32> m_nBackend = 0;
    property_variable<tInt32> m_nTarget = 0;

    property_variable<bool> m_bBlur = false;
    property_variable<bool> m_bDraw = true;

    Ptr<FaceDetectorYN> m_pDetector;
    
    cPinWriter* m_pFaceWriter;

public:
    struct tFace
    {
        int32_t x, y, width, height;
    };

public:
    
    cFaceDetectorFilter()
    {
        m_strFDModel.SetDescription("Path to the model. Download yunet.onnx in https://github.com/opencv/opencv_zoo/tree/master/models/face_detection_yunet (face_detection_yunet_2021dec.onnx)");
        m_strFDModel.SetFilenameExtensionFilter("ONNX (*.onnx)");
        RegisterPropertyVariable("fd_model", m_strFDModel);

        m_strFRModel.SetDescription("Path to the face recognition model. Download the model at https://github.com/opencv/opencv_zoo/tree/master/models/face_recognition_sface (face_recognition_sface_2021dec.onnx)");
        m_strFRModel.SetFilenameExtensionFilter("ONNX (*.onnx)");
        RegisterPropertyVariable("fr_model", m_strFRModel);
        
        m_fScoreThreshold.SetDescription("Filter out faces of score < score_threshold");
        RegisterPropertyVariable("score_threshold", m_fScoreThreshold);

        m_fNMSThreshold.SetDescription("Suppress bounding boxes of iou >= nms_threshold");
        RegisterPropertyVariable("nms_threshold", m_fNMSThreshold);

        m_fScale.SetDescription("Scale factor used to resize input video frames");
        RegisterPropertyVariable("scale", m_fScale);

        m_nTopK.SetDescription("Keep top_k bounding boxes before NMS");
        RegisterPropertyVariable("top_k", m_nTopK);

        m_nBackend.SetDescription("Enum of computation backends supported by layers.");
        m_nBackend.SetValueList({
            {cv::dnn::DNN_BACKEND_DEFAULT, "DNN_BACKEND_DEFAULT"},
            {cv::dnn::DNN_BACKEND_HALIDE, "DNN_BACKEND_HALIDE"},
            {cv::dnn::DNN_BACKEND_INFERENCE_ENGINE, "DNN_BACKEND_INFERENCE_ENGINE"},
            {cv::dnn::DNN_BACKEND_OPENCV, "DNN_BACKEND_OPENCV"},
            {cv::dnn::DNN_BACKEND_VKCOM, "DNN_BACKEND_VKCOM"},
            {cv::dnn::DNN_BACKEND_CUDA, "DNN_BACKEND_CUDA"},
            });
        RegisterPropertyVariable("backend", m_nBackend);

        m_nTarget.SetDescription("Enum of target devices for computations.");
        m_nTarget.SetValueList({
            {cv::dnn::DNN_TARGET_CPU, "DNN_TARGET_CPU"},
            {cv::dnn::DNN_TARGET_OPENCL, "DNN_TARGET_OPENCL"},
            {cv::dnn::DNN_TARGET_OPENCL_FP16, "DNN_TARGET_OPENCL_FP16"},
            {cv::dnn::DNN_TARGET_MYRIAD, "DNN_TARGET_MYRIAD"},
            {cv::dnn::DNN_TARGET_VULKAN, "DNN_TARGET_VULKAN"},
            {cv::dnn::DNN_TARGET_FPGA, "DNN_TARGET_FPGA"},
            {cv::dnn::DNN_TARGET_CUDA, "DNN_TARGET_CUDA"},
            {cv::dnn::DNN_TARGET_CUDA_FP16, "DNN_TARGET_CUDA_FP16"},
            });
        RegisterPropertyVariable("target", m_nTarget);

        SetDescription("Use this filter to create and manipulate comprehensive artificial neural networks.");
        SetHelpLink("$(ADTF_OPENCV_TOOLBOX_DIR)/doc/html/opencv_components_plugin_opencv_dnn_filter.html");


        auto oFaceType = adtf::mediadescription::structure<tFace>("tFace")
            .Add("x", &tFace::x)
            .Add("y", &tFace::y)
            .Add("width", &tFace::width)
            .Add("height", &tFace::height);

        m_pFaceWriter = CreateOutputPin("faces", oFaceType);
        SetDescription("faces", "tFace sample per detected face");

        m_bBlur.SetDescription("Blur faces");
        RegisterPropertyVariable("blur", m_bBlur);

        m_bDraw.SetDescription("Draw face boundery and dot's");
        RegisterPropertyVariable("draw", m_bDraw);
    }
    
    ~cFaceDetectorFilter() override
    {
        
    }

    tResult OnStagePreConnect() override
    {
        if (!cFileSystem::Exists(m_strFDModel))
        {
            RETURN_ERROR_DESC(ERR_NOT_FOUND, "Could not find fd model at %s", m_strFDModel->GetPtr());
        }

        m_pDetector = FaceDetectorYN::create(m_strFDModel->GetPtr(), "", Size(320, 320), m_fScoreThreshold, m_fNMSThreshold, m_nTopK, m_nBackend, m_nTarget);

        RETURN_NOERROR;
    }

    tResult ProcessMat(::cv::Mat const& oSrcMat, ::adtf::base::tNanoSeconds oTimestamp, ::std::string oSrcFormatName = "") override
    {
        if (!m_pDetector)
        {
            THROW_ERROR_DESC(ERR_NO_CLASS, "Forgot to call OnStagePreConnect()");
        }

        {
            EASY_BLOCK("Reset size");
            m_pDetector->setInputSize(oSrcMat.size());
        }

        cv::Mat oFaces1;
        {
            EASY_BLOCK("Detect");
            // Assuming that detect() does not change input oResult.
            m_pDetector->detect(oSrcMat, oFaces1);
        }

        cv::Mat oDstMat = oSrcMat;

        if (m_bBlur)
        {
            EASY_BLOCK("Blur");

            // Clone before drawing, if necessary
            if (oDstMat.data == oSrcMat.data)
            {
                oDstMat = oSrcMat.clone();
            }

            BlurFaces(oDstMat, oFaces1);
        }

        if(m_bDraw)
        {
            EASY_BLOCK("Visualize");

            // Clone before drawing, if necessary
            if (oDstMat.data == oSrcMat.data)
            {
                oDstMat = oSrcMat.clone();
            }

            Visualize(oDstMat, oFaces1);
        }

        {
            EASY_BLOCK("Generate Face Samples");
            WriteDetectedFaces(oFaces1, oTimestamp);
        }

        RETURN_IF_FAILED(WriteMat(m_pOutput, oDstMat, oTimestamp, oSrcFormatName));
        RETURN_NOERROR;
    }

    void Visualize(const Mat& input, Mat& faces, int thickness = 2)
    {
        for (int i = 0; i < faces.rows; i++)
        {
            // Draw bounding box
            rectangle(input, Rect2i(int(faces.at<float>(i, 0)), int(faces.at<float>(i, 1)), int(faces.at<float>(i, 2)), int(faces.at<float>(i, 3))), Scalar(0, 255, 0), thickness);
            // Draw landmarks
            circle(input, Point2i(int(faces.at<float>(i, 4)), int(faces.at<float>(i, 5))), 2, Scalar(255, 0, 0), thickness);
            circle(input, Point2i(int(faces.at<float>(i, 6)), int(faces.at<float>(i, 7))), 2, Scalar(0, 0, 255), thickness);
            circle(input, Point2i(int(faces.at<float>(i, 8)), int(faces.at<float>(i, 9))), 2, Scalar(0, 255, 0), thickness);
            circle(input, Point2i(int(faces.at<float>(i, 10)), int(faces.at<float>(i, 11))), 2, Scalar(255, 0, 255), thickness);
            circle(input, Point2i(int(faces.at<float>(i, 12)), int(faces.at<float>(i, 13))), 2, Scalar(0, 255, 255), thickness);
        }
    }

    void WriteDetectedFaces(const cv::Mat& oFaces, adtf::base::tNanoSeconds nTimestamp)
    {
        for (int i = 0; i < oFaces.rows; i++)
        {
            // bounding box
            output_sample_data<tFace> oFace(nTimestamp);
            oFace->x = int(oFaces.at<float>(i, 0));
            oFace->y = int(oFaces.at<float>(i, 1));
            oFace->width = int(oFaces.at<float>(i, 2));
            oFace->height = int(oFaces.at<float>(i, 3));

            m_pFaceWriter->Write(oFace.Release());
        }
    }

    void BlurFaces(cv::Mat& oImage, cv::Mat& oFaces)
    {
        for (int i = 0; i < oFaces.rows; i++)
        {
            try
            {
                // bounding box
                Rect oFace;
                oFace.x = int(oFaces.at<float>(i, 0));
                oFace.y = int(oFaces.at<float>(i, 1));
                oFace.width = int(oFaces.at<float>(i, 2));
                oFace.height = int(oFaces.at<float>(i, 3));

                cv::GaussianBlur(oImage(oFace), oImage(oFace), cv::Size(15, 15), 0);
            }
            catch (...)
            {
                LOG_RESULT(current_exception_to_result());
            }
        }
    }
};
