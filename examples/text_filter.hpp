/**
 *
 * @file
 * Copyright 2024 Digitalwerk GmbH.
 *
 *     This Source Code Form is subject to the terms of the Mozilla
 *     Public License, v. 2.0. If a copy of the MPL was not distributed
 *     with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * If it is not possible or desirable to put the notice in a particular file, then
 * You may include the notice in a location (such as a LICENSE file in a
 * relevant directory) where a recipient would be likely to look for such a notice.
 *
 * You may add additional accurate notices of copyright ownership.
 */

#include <adtffiltersdk/adtf_filtersdk.h>
#include <adtfsystemsdk/adtf_systemsdk.h>

#include <opencv_base_filter/opencv_base_filter.h>

#include <opencv2/opencv.hpp>

using namespace adtf::util;
using namespace adtf::ucom;
using namespace adtf::base;
using namespace adtf::streaming;
using namespace adtf::filter;
using namespace adtf::system;
using namespace adtf::opencvtb;

class cTextFilter : public cOpenCVBaseFilter
{
public:
    ADTF_CLASS_ID_NAME(cTextFilter,
        "opencv_text.filter.dw.cid",
        "OpenCV Text Filter");

public:
    cTextFilter()
    {
        SetDescription("Use this filter to draw text to images.");
        SetHelpLink("$(ADTF_OPENCV_TOOLBOX_DIR)/doc/html/opencv_components_plugin_opencv_text_filter.html");

        // Shouldnt this be coord x?
        m_nHeight.SetDescription("Set height of text in percent");
        RegisterPropertyVariable("height", m_nHeight);

        // Shouldnt this be coord y?
        m_nWidth.SetDescription("Set width of text in percent");
        RegisterPropertyVariable("width", m_nWidth);

        m_pReader = CreateInputPin("number");
        SetDescription("number", "The number drawn to the image as string");
    }

    tResult ProcessInput(ISampleReader* pReader,
        const iobject_ptr<const ISample>& pSample) override
    {
        if (pReader == m_pReader)
        {
            sample_data<uint32_t> oData(pSample);
            m_strText = std::to_string(*oData);
        }
        else
        {
            object_ptr<const ISample> pTextSample;
            if (IS_OK(m_pReader->GetLastSample(pTextSample)))
            {
                sample_data<uint32_t> oData(pTextSample);
                m_strText = std::to_string(*oData);
            }

            return cOpenCVBaseFilter::ProcessInput(pReader, pSample);
        }

        RETURN_NOERROR;
    }

    tResult ProcessMat(::cv::Mat const& oSrcMat, ::adtf::base::tNanoSeconds oTimestamp, ::std::string oSrcFormatName = "") override
    {
        // Always clone to leave input unchanged.
        cv::Mat oDstMat = oSrcMat.clone();

        // Strange 'bottomLeft' position.
        cv::Point oPoint(static_cast<int>(oDstMat.cols * *m_nWidth), static_cast<int>(oDstMat.rows * *m_nHeight));
        cv::putText(oDstMat, m_strText, oPoint, cv::FONT_HERSHEY_SIMPLEX, 2.0, cv::Scalar(244.0, 244.5, 244.5), 3);
        RETURN_IF_FAILED(WriteMat(m_pOutput, oDstMat, oTimestamp, oSrcFormatName));
        RETURN_NOERROR;
    }

private:
    property_variable<double> m_nHeight = 0.5;
    property_variable<double> m_nWidth = 0.5;

    std::string m_strText = "n/a";
    ISampleReader* m_pReader;
};

class cTriggerCounter : public cFilter
{
public:
    ADTF_CLASS_ID_NAME(cTriggerCounter,
        "trigger_counter.filter.dw.cid",
        "Trigger Counter Filter");

public:
    cTriggerCounter()
    {
        SetDescription("Use this filter to count trigger events and read the counter.");
        SetHelpLink("$(ADTF_OPENCV_TOOLBOX_DIR)/doc/html/opencv_components_plugin_trigger_counter_filter.html");

        m_pWriter = CreateOutputPin("counter", stream_type_plain<uint32_t>());
        SetDescription("counter", "Amount of trigger");

        CreateRunner("trigger", [this](adtf::base::tNanoSeconds nTimestamp) -> tResult
        {
            m_nCounter++;
            return m_pWriter->Write(output_sample_data<uint32_t>(nTimestamp, m_nCounter).Release());
        });
        SetDescription("trigger", "Trigger to be counted");
    }


private:
    uint32_t m_nCounter = 0;
    ISampleWriter* m_pWriter;
};
