/**
 *
 * @file
 * Copyright 2024 Digitalwerk GmbH.
 *
 *     This Source Code Form is subject to the terms of the Mozilla
 *     Public License, v. 2.0. If a copy of the MPL was not distributed
 *     with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * If it is not possible or desirable to put the notice in a particular file, then
 * You may include the notice in a location (such as a LICENSE file in a
 * relevant directory) where a recipient would be likely to look for such a notice.
 *
 * You may add additional accurate notices of copyright ownership.
 */

#include <adtffiltersdk/adtf_filtersdk.h>
#include <adtfsystemsdk/adtf_systemsdk.h>

#include <opencv_base_filter/opencv_base_filter.h>

#include <opencv2/opencv.hpp>
#include <opencv2/dnn.hpp>

using namespace adtf::util;
using namespace adtf::ucom;
using namespace adtf::base;
using namespace adtf::streaming;
using namespace adtf::filter;
using namespace adtf::system;

using namespace cv::dnn;
using namespace cv;

using namespace adtf::opencvtb;

class cResizeFilter : public cOpenCVBaseFilter
{
public:
    ADTF_CLASS_ID_NAME(cResizeFilter,
        "opencv_image_resizer.filter.dw.cid",
        "OpenCV Image Resizer");

    property_variable<int> m_nWidth = 400;
    property_variable<int> m_nHeight = 400;

public:
    cResizeFilter()
    {
        m_nWidth.SetDescription("Desired width in pixel after resizing.");
        RegisterPropertyVariable("width", m_nWidth);
        m_nHeight.SetDescription("Desired height in pixel after resizing.");
        RegisterPropertyVariable("height", m_nHeight);

        SetDescription("Use this filter to resize images to a specified size in pixels.");
        SetHelpLink("$(ADTF_OPENCV_TOOLBOX_DIR)/doc/html/opencv_components_plugin_opencv_image_resizer.html");
    }

    tStreamImageFormat ConvertImageFormat(const tStreamImageFormat & oImageFormat) override
    {
        size_t bytesPerPixel = 0;
        if (oImageFormat.m_strFormatName == ADTF_IMAGE_FORMAT(RGB_24) ||
            oImageFormat.m_strFormatName == ADTF_IMAGE_FORMAT(BGR_24))
        {
            bytesPerPixel = 3;
        }
        else if (oImageFormat.m_strFormatName == ADTF_IMAGE_FORMAT(GREYSCALE_8))
        {
            bytesPerPixel = 1;
        }

        if (bytesPerPixel < 1)
        {
            std::ostringstream o;
            o << "ResizeFilter does not support this format '" << oImageFormat.m_strFormatName.GetPtr() << "'";
            THROW_ERROR_DESC(ERR_INVALID_ARG, o.str().c_str());
        }

        tStreamImageFormat oFormat = oImageFormat;
        oFormat.m_ui32Width = m_nWidth;
        oFormat.m_ui32Height = m_nHeight;
        oFormat.m_szMaxByteSize = bytesPerPixel * m_nWidth * m_nHeight; // Still wrong because should be Mat.step instead of bpp * width;
        return oFormat;
    }
    
    tResult ProcessMat(::cv::Mat const& oSrcMat, ::adtf::base::tNanoSeconds oTimestamp, ::std::string oSrcFormatName = "") override
    {
        if ((oSrcMat.cols == m_nWidth) && (oSrcMat.rows == m_nHeight))
        {
            RETURN_IF_FAILED(WriteMat(m_pOutput, oSrcMat, oTimestamp, oSrcFormatName)); // Relay candidate.
            RETURN_NOERROR;
        }

        // Now resize() has something todo...
        cv::Mat oDstMat;
        resize(oSrcMat, oDstMat, Size(m_nWidth, m_nHeight));
        RETURN_IF_FAILED(WriteMat(m_pOutput, oDstMat, oTimestamp, oSrcFormatName));
        RETURN_NOERROR;
    }
};
