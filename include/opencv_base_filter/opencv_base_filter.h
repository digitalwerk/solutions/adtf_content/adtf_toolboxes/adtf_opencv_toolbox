/**
 *
 * @file
 * Copyright 2024 Digitalwerk GmbH.
 *
 *     This Source Code Form is subject to the terms of the Mozilla
 *     Public License, v. 2.0. If a copy of the MPL was not distributed
 *     with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * If it is not possible or desirable to put the notice in a particular file, then
 * You may include the notice in a location (such as a LICENSE file in a
 * relevant directory) where a recipient would be likely to look for such a notice.
 *
 * You may add additional accurate notices of copyright ownership.
 */

#pragma once
#include <opencv_base_filter/opencv_samplebuffer.h>
#include <adtffiltersdk/adtf_filtersdk.h>

namespace adtf {
namespace opencvtb {

/**
 *  cOpenCVBaseFilter is the base class for all OpenCV filter using the ProcessMat() interface.
 */   
class cOpenCVBaseFilter : public ::adtf::filter::cFilter
{
protected:
    //! The output pin 'mat_out' 
    ::adtf::filter::cPinWriter* m_pOutput;
    //! The input pin 'mat_in'
    ::adtf::filter::cPinReader* m_pInput;
    //! The input sample. We inject this in relay case to WriteMat. Will be reset after ProcessMat().
    ::adtf::ucom::object_ptr<const ::adtf::streaming::ISample> m_pInputSample;
    //! The input Mat. We store it to simplify API (one less param). Will be reset after ProcessMat().
    ::cv::Mat m_oInputMat;
    //! The input stream format.
    ::adtf::streaming::tStreamImageFormat m_sInputFormat;
    //! A switch to control behaviour.
    ::adtf::base::property_variable<tBool> m_bOnlyLastSample = false;
    //! The OpenCV Mat type.
    std::optional<uint32_t>  m_nMatType;

public:
    /**
        * Default constructor
        */
    cOpenCVBaseFilter();

    /**
        * Write/Send OpenCV data between image filters. With pins of type <adtf/image>.
        *
        * @param[in] pPin The output-pin we like to write <adtf/image> to.
        * @param[in] oMat The OpenCV Mat we like to write as type <adtf/image>.
        * @param[in] oTimestamp The output timestamp.
        * @param[in] oFormatName The output image-format-name, e.g. ADTF_IMAGE_FORMAT(BGR_24),
        *  OpenCV does not store a compatible image pixel format, it must be transmitted extra to cv::Mat.
        *  If 'oFormatName' is empty, then the last image-format-name is used, if any.
        *  If 'oFormatName' is still empty then auto-detection occurs using given cv::Mat.
        *  If 'oFormatName' is still empty then exception is thrown (ERR_FAILED).
        *
        * @return Standard error code.
        *
        * @details Use WriteMat() inside @ref ProcessMat() to send OpenCV data to other filters.
        */
    virtual tResult WriteMat(
        ::adtf::filter::cPinWriter* pPin, 
        ::cv::Mat const & oMat,
        ::adtf::base::tNanoSeconds oTimestamp,
        ::std::string const & oFormatName);

    /**
        * ProcessMat() and ConvertImageFormat() are the new interface and reason for this class.
        * 
        * Creates output stream format from input stream format.
        * Is done initially in ctr of filter (atleast once, if any).
        * Shall be overriden by all filters that change the format while processing.
        * 
        * @param[in] oImageFormat The input stream format.
        *
        * @return Output stream format of the filter.
        */
    virtual ::adtf::streaming::tStreamImageFormat ConvertImageFormat(const ::adtf::streaming::tStreamImageFormat & oImageFormat);

    /**
        * ProcessMat() and ConvertImageFormat() are the new interface and reason for this class.
        * 
        * Any override must guarantee to clone the input 'oSrcMat' before any changes are made. ( e.g. drawing to it )
        * Filter without need to modify given input 'oSrcMat' are relay candidates.
        * e.g. Filter that only analyze or display the input 'oSrcMat' without changing original.
        *
        * @param[in] oSrcMat The input OpenCV Mat to be processed/filtered by this filter class.
        * @param[in] oTimestamp The input timestamp.
        * @param[in] oSrcFormatName The input image-format-name (backed by OpenCV Mat).
        *            Passing empty string is more costly due to triggered auto-image-format-detection.
        *
        * @details Inside ProcessMat() use @see WriteMat() to write cv::Mat data to output-pins.
        *          WriteMat(nullptr) -> Send to 'mat_out' standard output-pin (of this base-class). ADTF/image type.
        *          WriteMat(pAnyPin) -> Send OpenCV Mat to user provided output-pin (of derived class). Custom type.
        *
        * @return Standard error code.
        *
        */
    virtual tResult ProcessMat( ::cv::Mat const & oSrcMat, ::adtf::base::tNanoSeconds oTimestamp, ::std::string oSrcFormatName = "" ) = 0;

    /**
        * New entry point called at Init() phase.
        *
        * @return Standard result code.
        */
    virtual tResult OnStageFirst() { RETURN_NOERROR; };

    /**
        * New entry point called at Init() phase.
        *
        * @return Standard result code.
        */
    virtual tResult OnStagePreConnect() { RETURN_NOERROR; };

    /**
        * New entry point called at Init() phase.
        *
        * @return Standard result code.
        */
    virtual tResult OnStagePostConnect() { RETURN_NOERROR; };

    /**
        * Processes incoming samples.
        *
        * @param[in] pReader The input sample queue
        * @param[in] pSample The input sample
        *
        * @return Standard result code.
        */
    tResult ProcessInput(::adtf::streaming::ISampleReader* pReader,
        ::adtf::ucom::iobject_ptr<const adtf::streaming::ISample> const& pSample) override;

    /**
        * Initializes the filter.
        *
        * @param[in] eStage The input state.
        *
        * @return Standard result code.
        */
    tResult Init(::adtf::streaming::ant::cFilterLevelmachine::tInitStage eStage) override;
};

} // end namespace opencvtb.
} // end namespace adtf.
