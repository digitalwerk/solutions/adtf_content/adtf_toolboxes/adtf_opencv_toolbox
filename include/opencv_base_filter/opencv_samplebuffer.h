/**
 *
 * @file
 * Copyright 2024 Digitalwerk GmbH.
 *
 *     This Source Code Form is subject to the terms of the Mozilla
 *     Public License, v. 2.0. If a copy of the MPL was not distributed
 *     with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * If it is not possible or desirable to put the notice in a particular file, then
 * You may include the notice in a location (such as a LICENSE file in a
 * relevant directory) where a recipient would be likely to look for such a notice.
 *
 * You may add additional accurate notices of copyright ownership.
 */

#pragma once
#include <opencv2/opencv.hpp>
#include <opencv2/core/mat.hpp>
#include <adtf_streaming3.h>

namespace adtf {
namespace opencvtb {

/**
 *   This function simplifies creating a new sample out of a given timestamp and given OpenCV Mat.
 *   A new sample-buffer is created with given OpenCV Mat and attached to the sample.
 *   @param [out] oSample A reference to a sample object_ptr.
 *   @param [in] oMat An OpenCV mat to be attached to the newly created sample and sampleBuffer.
 *   @param [in] oTimestamp A timestamp attached to the newly created sample.
 *   @return Standard result.
 */
tResult createOpenCVSample(
    ::adtf::ucom::object_ptr<::adtf::streaming::ISample> & oSample,
    ::cv::Mat const & oMat, 
    ::adtf::base::tNanoSeconds const & oTimestamp );

/**
 * Helper function to compare two tStreamImageFormat. Helps reducing bloated code.
 *
 * @param[in] a The first tStreamImageFormat
 * @param[in] b The second tStreamImageFormat
 *
 * @return Returns true if all members (except endianess) are equal, else false.
 */
bool isSameFormat( 
    ::adtf::streaming::tStreamImageFormat const& a,
    ::adtf::streaming::tStreamImageFormat const& b);

/**
 * Helper function to create compatible ADTF image stream format from given cv::Mat.
 *
 * @param[in] oMat OpenCV Mat.
 *
 * @return ADTF compatible image stream format, if any.
 *         Returns "" empty string as format-name when no compatible image-format-name was found for given cv::Mat.
 *         Should be checked by caller. This can happen for special DeepNeuralNetwork buffers, etc...
 */
::adtf::streaming::tStreamImageFormat createCompatibleImageStreamFormat(
    ::cv::Mat const& oMat);

} // end namespace opencvtb.
} // end namespace adtf.

